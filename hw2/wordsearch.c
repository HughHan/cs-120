/**
 * 	Hugh Han
 *	EN.600.120 Intermediate Programming, Spring 2015
 * 	HW 2: wordsearch.c
 *	
 *	February 16, 2015
 *	Phone: (516) 707-1344
 *	JHED ID: hhan17
 *	JHU Email: hhan17@jhu.edu
 */

#include "wordsearch.h"

// NOTE: all the functions declared in wordsearch.h should be
// implemented in this file.

int load_grid(FILE* infile, char outgrid[][MAXSZ]) {
	
	int r = 0;
	int c = 0;
	int col = 0;
	int tempC = 0;
	
	char in;
	while ((in = fgetc(infile)) != EOF) {
		if (in == '\n') {
			col = c;		
			if (r == 0)
				tempC = c;
			if (tempC != c) // if inconsistent number of columns
				return 0;
			r++;
			c = 0;
		}
		else {
			if (in != '\0')
				outgrid[r][c++] = tolower(in);
		}
	}

	if (r > MAXSZ || col > MAXSZ || r <= 0 || col <= 0) {
		return 0;
	}
	if (r == col)
		return r;
	return 0;

}

bool has_next_char(char grid[][MAXSZ], char c, int i, int j) {
	return grid[i][j] == c;
}

bool is_length_valid(int l, int n, int i, int j, char direction) {
	if (l > n)
		return false;
	switch (direction) {
		case 'u': return (l <= i+1); 		break;
		case 'd': return ((i + l) <= n);	break;
		case 'l': return (l <= j+1);		break;
		case 'r': return ((j + l) <= n);	break;
	}
	return false; /* IS THIS STATEMENT OKAY?? */
}

bool find_word_up(char grid[][MAXSZ], int n, char w[], int i, int j) {
	int length = strlen(w);
	if (is_length_valid(length, n, i, j, 'u')) {
		bool has_word = true;
		int k;
		for (k = 0; k < length; k++) {
			has_word = has_next_char(grid, w[k], i--, j);
			if (!has_word)
				return false;
		}
		return has_word;
	}
	return false;
}

bool find_word_down(char grid[][MAXSZ], int n, char w[], int i, int j) {
	int length = strlen(w);
	if (is_length_valid(length, n, i, j, 'd')) {
		bool has_word = true;
		int k;
		for (k = 0; k < length; k++) {
			has_word = has_next_char(grid, w[k], i++, j);
			if (!has_word)
				return false;
		}
		return has_word;
	}
	return false;
}

bool find_word_left(char grid[][MAXSZ], int n, char w[], int i, int j) {
	int length = strlen(w);
	if (is_length_valid(length, n, i, j, 'l')) {
		bool has_word = true;
		int k;
		for (k = 0; k < length; k++) {
			has_word = has_next_char(grid, w[k], i, j--);
			if (!has_word)
				return false;
		}
		return has_word;
	}
	return false;
}

bool find_word_right(char grid[][MAXSZ], int n, char w[], int i, int j) {
	int length = strlen(w);
	if (is_length_valid(length, n, i, j, 'r')) {
		bool has_word = true;
		int k;
		for (k = 0; k < length; k++) {
			has_word = has_next_char(grid, w[k], i, j++);
			if (!has_word)
				return false;
		}
		return has_word;
	}
	return false;
}

/** 
 *	searches the n x n grid for the given word, w, and prints
 *  the matches to outfile; n specifies the length and width
 *  of the grid (which is square)
 */
void write_all_matches(FILE* outfile, char grid[][MAXSZ], int n, char w[]) { 
	bool word_found = false;
	int r;
	int c;
	for (r = 0; r < n; r++) {
		for (c = 0; c < n; c++) {
			if (find_word_up(grid, n, w, r, c)) {
				word_found = true;
//				printf("%s %d %d U\n", w, r, c);
				fprintf(outfile, "%s %d %d U\n", w, r, c);
			}
			if (find_word_down(grid, n, w, r, c)) {
				word_found = true;
//				printf("%s %d %d D\n", w, r, c);
				fprintf(outfile, "%s %d %d D\n", w, r, c);
			}
			if (find_word_left(grid, n, w, r, c)) {
				word_found = true;
//				printf("%s %d %d L\n", w, r, c);
				fprintf(outfile, "%s %d %d L\n", w, r, c);
			}
			if (find_word_right(grid, n, w, r, c)) {
				word_found = true;
//				printf("%s %d %d R\n", w, r, c);
				fprintf(outfile, "%s %d %d R\n", w, r, c);
			}
		}
	}
	if (!word_found)
		fprintf(outfile, "%s - Not Found", w);
}
