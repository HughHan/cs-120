/**
 * 	Hugh Han
 *	EN.600.120 Intermediate Programming, Spring 2015
 * 	HW 2: test_wordsearch.c
 *	
 *	February 16, 2015
 *	Phone: (516) 707-1344
 *	JHED ID: hhan17
 *	JHU Email: hhan17@jhu.edu
 */

#include "wordsearch.h"

/*
 * Reads lhs and rhs character by character until either reaches eof.
 * Returns true if the files can be read and the two files match
 * character by character. Returns false if two files either can't be
 * opened or don't match.
 */
bool fileeq(char lhsName[], char rhsName[]) {
    FILE* lhs = fopen(lhsName, "r");
    FILE* rhs = fopen(rhsName, "r");

    // don't compare if we can't open the files
    if (!lhs || !rhs) return false;

    bool match = true;
    // read until both of the files are done or there is a mismatch
    while (!feof(lhs) || !feof(rhs)) {
        if (feof(lhs) ||                  // lhs done first
            feof(rhs) ||                  // rhs done first
            (fgetc(lhs) != fgetc(rhs))) { // chars don't match
            match = false;
            break;
        }
    }
    fclose(lhs);
    fclose(rhs);
    return match;
}

/*
 * Test fileeq on same file, files with same contents, files with
 * different contents and a file that doesn't exist
 */
void test_fileeq() {
    FILE* fptr = fopen("test1.txt", "w");
    fprintf(fptr, "this\nis\na test\n");
    fclose(fptr);

    fptr = fopen("test2.txt", "w");
    fprintf(fptr, "this\nis\na different test\n");
    fclose(fptr);

    fptr = fopen("test3.txt", "w");
    fprintf(fptr, "this\nis\na test\n");
    fclose(fptr);

    assert (fileeq("test1.txt", "test1.txt"));
    assert (fileeq("test2.txt", "test2.txt"));
    assert (!fileeq("test2.txt", "test1.txt"));
    assert (!fileeq("test1.txt", "test2.txt"));
    assert (fileeq("test3.txt", "test3.txt"));
    assert (fileeq("test1.txt", "test3.txt"));
    assert (fileeq("test3.txt", "test1.txt"));
    assert (!fileeq("test2.txt", "test3.txt"));
    assert (!fileeq("test3.txt", "test2.txt"));
    assert (!fileeq("", ""));  // can't open file
}


/*
 * writes out a hard-coded grid to a file;
 * uses read_grid to read in the grid;
 * test that the loaded grid is correct
 */
void test_read_grid() {
	printf("Running load_grid()...\n");

	FILE* fptr = fopen("grid.txt", "r");
    char grid[MAXSZ][MAXSZ];
	load_grid(fptr, grid);
}

void test_find_forward() { 

	FILE* fptr = fopen("grid.txt", "r");
    char grid[MAXSZ][MAXSZ];
    int n = load_grid(fptr, grid);
	int r;
	int c;
	for (r = 0; r < n; r++) {
		for (c = 0; c < n; c++) {
			printf("Running find_word_right()...\n");
			find_word_right(grid, n, "key", r, c);
		}
	}
	
	assert (find_word_right(grid, n, "key", 2, 1));
	assert (!find_word_right(grid, n, "tip", 0, 0));
	assert (!find_word_right(grid, n, "nope", 0, 0));
	assert (!find_word_right(grid, n, "this string is too long", 0, 0));
	
}

void test_find_backward() { 
    FILE* fptr = fopen("grid.txt", "r");
    char grid[MAXSZ][MAXSZ];
    int n = load_grid(fptr, grid);
	int r;
	int c;
	for (r = 0; r < n; r++) {
		for (c = 0; c < n; c++) {
			printf("Running find_word_left()...\n");
			find_word_left(grid, n, "tip", r, c);
		}
	} 
	
	assert (find_word_left(grid, n, "tip", 0, 2));
	assert (!find_word_left(grid, n, "pop", 0, 3));
	assert (!find_word_left(grid, n, "nope", 0, 3));
	assert (!find_word_left(grid, n, "this string is too long", 0, 3));
	
}

void test_find_up() { 
	FILE* fptr = fopen("grid.txt", "r");
    char grid[MAXSZ][MAXSZ];
    int n = load_grid(fptr, grid);
	int r;
	int c;
	for (r = 0; r < n; r++) {
		for (c = 0; c < n; c++) {
			printf("Running find_word_up()...\n");
			find_word_up(grid, n, "pop", r, c);
		}
	} 
	
	assert (find_word_up(grid, n, "pop", 2, 0));
	assert (!find_word_up(grid, n, "key", 3, 0));
	assert (!find_word_up(grid, n, "nope", 3, 0));
	assert (!find_word_up(grid, n, "this string is too long", 3, 0));
	
}

void test_find_down() { 
    FILE* fptr = fopen("grid.txt", "r");
    char grid[MAXSZ][MAXSZ];
    int n = load_grid(fptr, grid);
	int r;
	int c;
	for (r = 0; r < n; r++) {
		for (c = 0; c < n; c++) {
			printf("Running find_word_down()...\n");
			find_word_down(grid, n, "pop", r, c);
		}
	} 
	
	assert (find_word_down(grid, n, "pop", 0, 0));
	assert (!find_word_down(grid, n, "key", 0, 0));
	assert (!find_word_down(grid, n, "nope", 0, 0));
	assert (!find_word_down(grid, n, "this string is too long", 3, 3));
	
}
void test_write_all_matches() { 
	FILE* fptr = fopen("grid.txt", "r");
    char grid[MAXSZ][MAXSZ];
    int n = load_grid(fptr, grid);
	FILE* outfile = fopen("outfile.txt", "w");
	write_all_matches(outfile, grid, n, "tip");
	write_all_matches(outfile, grid, n, "pop");
    write_all_matches(outfile, grid, n, "key");
    fclose(outfile);
}


int main(void) {
    printf("Testing fileeq...\n");
    test_fileeq();
    printf("Passed fileeq tests.\n");

    printf("Running wordsearch tests...\n");
    test_read_grid();
    test_find_forward();
    test_find_backward();
    test_find_up();
    test_find_down();
    test_write_all_matches();
    printf("Passed wordsearch tests.\n");

    return 0;
}
