/**
 * 	Hugh Han
 *	EN.600.120 Intermediate Programming, Spring 2015
 * 	HW 2: hw2.c
 *	
 *	February 16, 2015
 *	Phone: (516) 707-1344
 *	JHED ID: hhan17
 *	JHU Email: hhan17@jhu.edu
 */

#include "wordsearch.h"

// NOTE: this file should not contain any functions other than main();
// other functions should go in wordsearch.c, with their prototypes in
// wordsearch.h

int main(int argc, char* argv[]) {

	if (argc == 1) {
		printf("USAGE: ./hw2");
	} else {
		char grid[MAXSZ][MAXSZ];
		FILE* fptr = fopen(argv[1], "r");
		FILE* outfile = fopen("outfile.txt", "w");
		int size = load_grid(fptr, grid);

		if (size == 0) {
			printf("The grid is invalid.\n");
			return 1;	// invalid grid, exits program
		} else {
			char input[100];
			scanf("%[^\n]%*c", input);
			int length = strlen(input);

			int i = 0;
			while (i < length) {

				int pos = 0;

				char w[MAXSZ] = "";
				while (input[i] != ' ' && input[i] != '\0') {
					w[pos++] = tolower(input[i]);
					i++;
				}
				while (input[i+1] == ' ') {
					i++;
				}
				if (strlen(w) > 0) {
					write_all_matches(outfile, grid, size, w);
					i++;
				}

			}
		}

		fclose(fptr);
		fclose(outfile);
		
	}

 	return 0;
}

