/**
 * 	Hugh Han
 *	EN.600.120 Intermediate Programming, Spring 2015
 * 	HW 2: wordsearch.h
 *	
 *	February 16, 2015
 *	Phone: (516) 707-1344
 *	JHED ID: hhan17
 *	JHU Email: hhan17@jhu.edu
 */

#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <assert.h>

// maximum grid size
#define MAXSZ 10


int load_grid(FILE* infile, char outgrid[][MAXSZ]);

int num_rows(char grid[][MAXSZ]);
int num_cols(char grid[][MAXSZ]);

bool has_next_char(char grid[][MAXSZ], char c, int i, int j);
bool is_length_valid(int l, int n, int i, int j, char direction);

bool find_word_up(char grid[][MAXSZ], int n, char w[], int i, int j);
bool find_word_down(char grid[][MAXSZ], int n, char w[], int i, int j);
bool find_word_left(char grid[][MAXSZ], int n, char w[], int i, int j);
bool find_word_right(char grid[][MAXSZ], int n, char w[], int i, int j);

void write_all_matches(FILE* outfile, char grid[][MAXSZ], int n, char w[]);
