/**
 *  Hugh Han
 *  EN.600.120 Intermediate Programming, Spring 2015
 *  HW 5: hw5.cpp
 *  
 *  March 29, 2015
 *  Phone: (516) 707-1344
 *  JHED ID: hhan17
 *  JHU Email: hhan17@jhu.edu
 */

#include "hw5.h"

using std::vector;
using std::unordered_map;
using std::string;
using std::cout;
using std::endl;

int main() {

	srand(time(NULL));
	
	LanguageModel model;
	vector<string> input;
	vector<string> output;

	read_in(&input);
	update_model(&model, input);
	generate_words(model, output);
	print_words(output);

	return 0;
	
}
