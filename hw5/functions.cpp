#include "hw5.h"

using std::vector;
using std::list;
using std::unordered_map;
using std::string;
using std::cin;
using std::cout;
using std::endl;

void read_in(vector<string>* words) {

	string line;
	string buffer;
	getline(cin, line);
	unsigned long pos = 0;

	words->push_back(START);
	while (pos < line.size()) {
		if (line.at(pos) != ' ') {
			buffer += line.at(pos);
		} else {
			words->push_back(buffer);
			buffer = "";
		}
		pos++;
	}
	words->push_back(buffer);
	words->push_back(END);

}

void update_model(LanguageModel* model, vector<string> words) {

	string curr_word;

	vector<string>::const_iterator v_it;
	for (v_it = words.begin(); v_it != words.end(); v_it++) {

		curr_word = *v_it;

		model->num_words[curr_word] += 1;						// increment the number of occurrences of a single word
		if ((v_it+1) != words.end()) {
			model->num_pairs[curr_word + " " + *(v_it+1)] += 1;	// concatenate two words next to each other, and increment their occurrence
			model->follows[curr_word].push_back(*(v_it+1));		// find the list that is mapped to the current word and add the word afterwards
			model->follows[curr_word].sort();
			model->follows[curr_word].unique();					// removes duplicate words that are directly next to each other (needs to be sorted)
		}

	}

}

void generate_words(LanguageModel model, vector<string>& words) {

	string curr_word = START;
	vector<string> poss_next;
	list<string>::const_iterator l_it;

	while (curr_word != END) {

		for (l_it = model.follows[curr_word].begin(); l_it != model.follows[curr_word].end(); l_it++) {	// iterate through possible following words
			for (int i = 0; i < model.num_pairs[curr_word + " " + *l_it]; i++) {						
				poss_next.push_back(*l_it);																// push next words as many times they occur
			}
		}

		curr_word = poss_next[rand() % poss_next.size()];
		words.push_back(curr_word);
		poss_next.clear();

	}

}

void print_words(vector<string> words) {

	vector<string>::const_iterator v_it;
	for (v_it = words.begin(); v_it != words.end(); v_it++) {
		if (*v_it != END) {
			cout << *v_it << " ";
		} else {
			cout << endl;
		}
	}

}
