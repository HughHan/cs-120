/**
 *  Hugh Han
 *  EN.600.120 Intermediate Programming, Spring 2015
 *  HW 5: hw5.h
 *  
 *  March 29, 2015
 *  Phone: (516) 707-1344
 *  JHED ID: hhan17
 *  JHU Email: hhan17@jhu.edu
 */

#include <list>
#include <vector>
#include <unordered_map>
#include <string>
#include <iostream>

#ifndef _HW5_H
#define _HW5_H

const std::string START = "<START>";
const std::string END = "<END>";

typedef struct s {
	std::unordered_map<std::string, int> num_words;
	std::unordered_map<std::string, int> num_pairs;
	std::unordered_map<std::string, std::list<std::string>> follows;
} LanguageModel;

void read_in(std::vector<std::string>* words);
void update_model(LanguageModel* model, std::vector<std::string> words);
void generate_words(LanguageModel model, std::vector<std::string>& words);
void print_words(std::vector<std::string> words);

#endif
