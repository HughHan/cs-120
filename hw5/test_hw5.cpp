/**
 *  Hugh Han
 *  EN.600.120 Intermediate Programming, Spring 2015
 *  HW 5: test_hw5.cpp
 *  
 *  March 29, 2015
 *  Phone: (516) 707-1344
 *  JHED ID: hhan17
 *  JHU Email: hhan17@jhu.edu
 */

#include "hw5.h"
#include <cassert>

using std::vector;
using std::string;
using std::cout;
using std::endl;

void test_update_model(LanguageModel* m, vector<string> words) {
    cout << "Testing update_model..." << endl;
    update_model(m, words);
    cout << "update_model test passed." << endl << endl;
}

void test_generate_words(LanguageModel m, vector<string> words) {
    cout << "Testing generate_words..." << endl;
    generate_words(m, words);
    cout << "generate_words test passed." << endl << endl;
}

void test_print_words(vector<string> words) {
    cout << "Testing print_words..." << endl;
    print_words(words);
    cout << "print_words test passed." << endl << endl;
}

void test_num_words(LanguageModel m) {

    assert(m.num_words[START] == 1);
    assert(m.num_words["he"] == 3);
    assert(m.num_words["liked"] == 2);
    assert(m.num_words["that"] == 1);
    assert(m.num_words["ate"] == 1);
    assert(m.num_words["the"] == 1);
    assert(m.num_words["sandwich"] == 1);
    assert(m.num_words[END] == 1);

}

void test_num_pairs(LanguageModel m) {

    assert(m.num_pairs[START + " he"] == 1);
    assert(m.num_pairs["he liked"] == 2);
    assert(m.num_pairs["liked that"] == 1);
    assert(m.num_pairs["that he"] == 1);
    assert(m.num_pairs["he ate"] == 1);
    assert(m.num_pairs["ate the"] == 1);
    assert(m.num_pairs["the sandwich"] == 1);
    assert(m.num_pairs["sandwich he"] == 1);
    assert(m.num_pairs["liked " + END] == 1);

}

void test_follows(LanguageModel m) {

    assert(m.follows[START].size() == 1);
    assert(m.follows["he"].size() == 3);
    assert(m.follows["liked"].size() == 2);
    assert(m.follows["that"].size() == 1);
    assert(m.follows["ate"].size() == 1);
    assert(m.follows["the"].size() == 1);
    assert(m.follows["sandwich"].size() == 1);
    assert(m.follows[END].size() == 0);

}

int main() {

    srand(time(NULL));

    vector<string> input = { START, "he", "liked", "that", "he", "ate", "the", "sandwich", "he", "liked", END };
    vector<string> output;
    LanguageModel m;

    cout << endl;

    test_update_model(&m, input);

    test_num_words(m);
    test_num_pairs(m);
    test_follows(m);

    test_generate_words(m, output);
    test_print_words(output);

    return 0;

}
