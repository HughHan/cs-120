/*
 * Hugh Han
 * William Yao
 *
 * EN.600.120 Intermediate Programming, Spring 2015
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define LENGTH 16

int main(int argc, char* argv[])
{
    char course[LENGTH];
    if (argc == 1) {
        return 1;  /* exit program */
    }

    FILE *inFile = fopen(argv[1], "r");
    FILE *outFile = fopen("courseInts.txt", "w");	
   
    while(fscanf(inFile, "%s", course) != EOF){

    	//printf("course string: %s\n", course);
	printf("%s", course);

    	/* Converting Division */
    	
    	long divisionInt; 
        char division[3];
        strncpy(division, course, 2);
        division[2]='\0';
        
        if (strcmp(division, "AS") == 0){
            divisionInt = 0;
        } else if (strcmp(division, "BU") == 0){
            divisionInt = 1;
        } else if (strcmp(division, "ED") == 0){
            divisionInt = 2;
        } else if (strcmp(division, "EN") == 0){
            divisionInt = 3;
        } else if (strcmp(division, "ME") == 0){
            divisionInt = 4;
        } else if (strcmp(division, "PH") == 0){
            divisionInt = 5;
        } else if (strcmp(division, "PY") == 0){
            divisionInt = 6;
        } else if (strcmp(division, "SA") == 0){
            divisionInt = 7;
        }
	//printf("%ld\n", divisionInt);
	
	divisionInt = divisionInt << 29;	

        /* Converting department */
        char department[4];
        strncpy(department, course + 3, 3);
        department [3] = '\0';
        int departmentInt = atoi(department);
        //printf("%d\n", departmentInt);
	departmentInt = departmentInt << 19;
	
       

	 /* Converting course */
        char newCourse[4];
        strncpy(newCourse, course + 7, 3);
        newCourse [3] = '\0';
        int newCourseInt = atoi(newCourse);
        //printf("%d\n", newCourseInt);
	newCourseInt = newCourseInt << 9;




	/* Converting Grade Part 1 */
        char gradeA[2];
        gradeA [1] = '\0';
        int gradeAInt;
        strncpy(gradeA, course+10, 1);


        if (strcmp(gradeA, "A") == 0){
            gradeAInt = 0;
        } else if (strcmp(gradeA, "B") == 0){
            gradeAInt = 1;
        } else if (strcmp(gradeA, "C") == 0){
            gradeAInt = 2;
        } else if (strcmp(gradeA, "D") == 0){
            gradeAInt = 3;
        } else if (strcmp(gradeA, "F") == 0){
            gradeAInt = 4;
        } else if (strcmp(gradeA, "I") == 0){
            gradeAInt = 5;
        } else if (strcmp(gradeA, "S") == 0){
            gradeAInt = 6;
        } else if (strcmp(gradeA, "U") == 0){
            gradeAInt = 7;
        }
	//printf("%d\n", gradeAInt);
        gradeAInt = gradeAInt << 6;

        /* Converting Grade A part 2 */
        char gradeB[2];
        gradeB [1] = '\0';
        int gradeBInt;
        strncpy(gradeB, course+11, 1);
        
        if (strcmp(gradeB, "+") == 0){
            gradeBInt = 0;
        } else if (strcmp(gradeB, "-") == 0){
            gradeBInt = 1;
        } else if (strcmp(gradeB, "/") == 0){
            gradeBInt = 2;
        }
	
	//printf("%d\n", gradeBInt);
        gradeBInt = gradeBInt << 4;
    
	 /* Converting Credit Part 1 */
        char creditA[2];
        
        strncpy(creditA, course+12,1);

        int creditAInt = atoi(creditA);
	
	//printf("%d\n", creditAInt);
	creditAInt = creditAInt << 1;

        /* Converting Credit Part 2 */
        char creditB[2];
        creditB [1] = '\0';
        strncpy(creditB, course+14,1);

        int creditBInt;
        
        if (strcmp(creditB, "0") == 0){
            creditBInt = 0;
        } else if (strcmp(creditB, "5") == 0){
            creditBInt = 1;
        }
	//printf("%d\n", creditBInt);

        /* Adding Total Shifted Decimals */
        //printf("%ld\n", divisionInt);
        //printf("%d\n", departmentInt);
        //printf("%d\n", newCourseInt);
        //printf("%d\n", gradeAInt);
        //printf("%d\n", gradeBInt);
        //printf("%d\n", creditAInt);
        //printf("%d\n", creditBInt);

	long totalD = divisionInt ^ departmentInt ^ newCourseInt ^ gradeAInt ^ gradeBInt ^ creditAInt ^ creditBInt;

	//printf("The Total Shifted Decimals is: (%ld)\n", totalD);
	
	printf("  %ld  ", totalD);
    	fprintf(outFile, "  %ld  ",totalD);

        /* Converting totalD to Binary */

        long num = totalD;
        int numArray[50];
        int i = 0;

        while (num > 0){
            numArray[i] = num % 2;
            num = num / 2;
            i++;
        
        }
	int decimals = i;
        while (decimals < 32){
	  printf("0");
	  decimals++;
	}
        for (int a = i - 1; a >=0; a--){
            printf( "%d", numArray[a]);
           // printf("%d",numArray[a]);
        }
	
	printf("\n");
	fprintf(outFile, "\n");      

	}
	fclose(outFile);
	return 0;

}



