/*
 * Hugh Han
 * William Yao
 *
 * EN.600.120 Intermediate Programming, Spring 2015
 * hw1b.c
 *
 */

#include <stdio.h>
#include <string.h>

#define INVALID_VALUE 12 // the number 12 will never appear anywhere in the array because 12 implies that the number of credits was 6.0, which is impossible. 
#define SIZE 2500
#define LENGTH 16

void print_main_menu() {
	printf	(
			"n - display the total number of courses\n"
			"d - list all courses from a particular department (prompt for number)\n"
			"l - list all courses with a particular letter grade  (prompt for grade)\n"
			"c - list all courses with at least a specified number of credits (prompt for credits)\n"
			"g - compute the GPA of all the courses with letter grades (skipping those with 'I', 'S' or 'U' grades)\n"
			"q - quit the program\n"
			"Enter letter choice -> "
		);
}

void print_formatted(int weird_number) {

	if (weird_number != INVALID_VALUE) {	

		int div		= (weird_number & 3758096384) >> 29; 	// 0b11100000000000000000000000000000
		int dept	= (weird_number & 536346624) >> 19;	// 0b00011111111110000000000000000000
		int crse	= (weird_number & 523776) >> 9; 	// 0b00000000000001111111111000000000
		int grade[2];
		grade[0]	= (weird_number & 448) >> 6;		// 0b00000000000000000000000111000000
		grade[1]	= (weird_number & 48) >> 4;		// 0b00000000000000000000000000110000
		int cred[2];
		cred[0]		= (weird_number & 14) >> 1;		// 0b00000000000000000000000000001110
		cred[1]		= weird_number & 1;			// 0b00000000000000000000000000000001
/*	
 		// DEBUG TOOL
		printf("div: %d\n", div);
		printf("dept: %d\n", dept);
		printf("crse: %d\n", crse);
		printf("grade: %d %d\n", grade[0], grade[1]);
		printf("cred: %d %d\n", cred[0], cred[1]);
*/
		char divf[3];
		switch (div) {
			case 0: divf[0] = 'A'; divf[1] = 'S'; break;
			case 1: divf[0] = 'B'; divf[1] = 'U'; break;
			case 2: divf[0] = 'E'; divf[1] = 'D'; break;
			case 3: divf[0] = 'E'; divf[1] = 'N'; break;
			case 4: divf[0] = 'M'; divf[1] = 'E'; break;
			case 5: divf[0] = 'P'; divf[1] = 'H'; break;
			case 6: divf[0] = 'P'; divf[1] = 'Y'; break;
			case 7: divf[0] = 'S'; divf[1] = 'A'; break;
		}
		divf[2] = '\0';
		
		char gradef[3];
		switch (grade[0]) {
			case 0: gradef[0] = 'A'; break;
			case 1: gradef[0] = 'B'; break;
			case 2: gradef[0] = 'C'; break;
			case 3: gradef[0] = 'D'; break;
			case 4: gradef[0] = 'F'; break;
			case 5: gradef[0] = 'I'; break;
			case 6: gradef[0] = 'S'; break;
			case 7: gradef[0] = 'U'; break;
		}
		switch (grade[1]) {
			case 0: gradef[1] = '+'; break; 
			case 1: gradef[1] = '-'; break;
			case 2: gradef[1] = '/'; break;
		}
		gradef[2] = '\0';
	
		int credf;
		if (cred[1] == 0)
			credf = 0;
		else
			credf = 5;
	
		// PRINT DIVISION
		printf("%s.", divf);
		
		// PRINT DEPARTMENT
		if (dept < 100) 	// makes sure that leading zeros are present
			printf("0");
		if (dept < 10) 		// makes sure that leading zeros are present
			printf("0");
		printf("%d.", dept);

		// PRINT COURSE NUMBER
		if (crse < 100) 	// makes sure that leading zeros are present
			printf("0");
		if (crse < 10) 		// makes sure that leading zeros are present
			printf("0");
		printf("%d", crse);

		// PRINT GRADE AND NUMBER OF CREDITS
		printf("%s%d.%d\n", gradef, cred[0], credf);

	}

}

void list_department(int array[SIZE]) {
	printf("Enter department number -> ");
	int dept;
	scanf("%d", &dept);
	getchar(); // skips over '\n'

	int i;
	for (i = 0; i < SIZE; i++) {
		if (dept == ((array[i] & 536346624 /* 0b00011111111110000000000000000000 */)) >> 19) {
			print_formatted(array[i]);
		}
	}
}

void list_grade(int array[SIZE]) {
	printf("Enter letter grade -> ");
	char gradeA;
	char gradeB;
	int gradeAInt;
	int gradeBInt;

	scanf("%c%c", &gradeA, &gradeB);
	getchar(); // skips over '\n'
	
//	printf("%c %c\n", gradeA, gradeB);
	
	char gradeAa[2];
	strncpy(gradeAa, &gradeA, 1);
	gradeAa[1] = '\0';
//	printf("%c is gradeA\n", gradeAa[0]);
	
	if(strcmp(gradeAa,"A") == 0){
		gradeAInt = 0;
	} else if(strcmp(gradeAa,"B") == 0){
                gradeAInt = 1;
	} else if(strcmp(gradeAa,"C") == 0){
                gradeAInt = 2;
        } else if(strcmp(gradeAa,"D") == 0){
                gradeAInt = 3;
        } else if(strcmp(gradeAa,"F") == 0){
                gradeAInt = 4;
        } else if(strcmp(gradeAa,"I") == 0){
                gradeAInt = 5;
        } else if(strcmp(gradeAa,"S") == 0){
                gradeAInt = 6;
        } else if(strcmp(gradeAa,"U") == 0){
                gradeAInt = 7;
        }
	
	char gradeBa[2];
        strncpy(gradeBa, &gradeB, 1);
        gradeBa[1] = '\0';
//	printf("%c is gradeB\n", gradeBa[0]);

	if(strcmp(gradeBa,"+") == 0){
                gradeBInt = 0;
        } else if(strcmp(gradeBa,"-") == 0){
                gradeBInt = 1;
        } else if(strcmp(gradeBa,"/") == 0){
                gradeBInt = 2;
        } 
//	printf("%c%c was read.\n", gradeA, gradeB);
//	printf("Numbers are %d and %d", gradeAInt, gradeBInt);
	int i;
	for (i = 0; i < SIZE; i++) {
		if ((gradeAInt == ((array[i] & 448) >> 6)) && (gradeBInt == ((array[i] & 48) >> 4))) {
			//printf("%d", array[i]);
			print_formatted(array[i]);
		}
	}
	
}

void list_credits(int array[SIZE]) {
    printf("Enter number of credits -> ");
    int cred[2];
    scanf("%d%*c%d", &cred[0], &cred[1]);
    getchar();
    
    int i;
    for (i = 0; i < SIZE; i++) {
        if (array[i] != INVALID_VALUE){
            if (cred[0] < ((array[i] & 14 ) >> 1)){
//		printf("weird number: %d\n", array[i]);
                print_formatted(array[i]);
            }
            if (cred[0] == ((array[i] & 14 ) >> 1)&&(cred[1] <= (array[i]))){
//		printf("weird number: %d\n", array[i]);
                print_formatted(array[i]);
            }
        }
    }   
}

void total_courses(int array[SIZE]) {
	int i;
	int count = 0;
	for (i = 0; i < SIZE; i++) {
		if (array[i] != INVALID_VALUE) {
			count += 1;
		}
	}
	printf("%d\n", count);
}

void compute_gpa(int array[SIZE]) {
	
	float grade;
	float gpa = 0.0;
	float count = 0.0;
	float temp;
	float weight;
	int i;
	for (i = 0; i < SIZE; i++) {
		if (array[i] != INVALID_VALUE) {
			if (((array[i] & 448) >> 6) < 5) { // if the grade is not I, S, or U
				switch ((array[i] & 448) >> 6) {
					case 0 /* 0b000 */: grade = 4.0; break;
					case 1 /* 0b001 */: grade = 3.0; break;
					case 2 /* 0b010 */: grade = 2.0; break;
					case 3 /* 0b011 */: grade = 1.0; break;
					case 4 /* 0b100 */: grade = 0.0; break;
				}
				switch ((array[i] & 48) >> 4) {
					case 0: if (grade != 4.0) // Assuming that A+ is a 4.0, not a 4.3 
							grade += 0.3; 
						break;
					case 1: if (grade != 0.0)  // Assuming that F- is a 0.0, not a -0.3 
							grade -= 0.3;
						break;
					case 2: break; // Do nothing
					
				}
				temp = (float) 0.5 * (array[i] & 1);	
				weight = ((array[i] & 14) >> 1) + temp;
				count += weight;
				gpa += (weight * grade);
			}
		}
	}
	gpa = gpa / count;
	printf("Cumulative GPA is: %.1f\n", gpa);
}

int main(void) {

	int courses[SIZE];
	char input_choice;
	FILE *fptr = fopen("courseInts.txt", "r");

	/* This sets all values in courses to INVALID_VALUE */	
	int i;
	for (i = 0; i < SIZE; i++) {
		courses[i] = INVALID_VALUE;
	}
	
	/* This sets the first values in the array to whatever integers were in courseInts.txt */	
	i = 0;
	do {	
		fscanf(fptr, "%d", &courses[i]);
		i++;
	} while (fgetc(fptr) != EOF);

	do {
		print_main_menu();
		input_choice = getchar();
		getchar(); // skips over '\n'
		switch (input_choice) {
			case 'n': total_courses(courses); break;
			case 'd': list_department(courses); break;
			case 'l': list_grade(courses); break;
			case 'c': list_credits(courses); break;
			case 'g': compute_gpa(courses); break;
			case 'q': /* Do nothing */ break;
			default: printf("Invalid choice."); break;
		}
	} while (input_choice != 'q');

	return 0;

}
