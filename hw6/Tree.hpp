/**
 *  Hugh Han
 *  EN.600.120 Intermediate Programming, Spring 2015
 *  HW 6C: Tree.hpp
 *  
 *	April 13, 2015
 *  Phone: (516) 707-1344
 *  JHED ID: hhan17
 *  JHU Email: hhan17@jhu.edu
 */

#ifndef _Tree_H
#define _Tree_H

#include <cstdlib>
#include <string>
#include <sstream>

template <class T>
class Tree {

	friend class TreeTempTest;

	T data; 
	Tree* kids; 
	Tree* sibs;
	Tree* prev;

public:

	/** Default constructor. */
	Tree(T data, Tree* kids = NULL, Tree* sibs = NULL, Tree* prev = NULL) {
		this->data = data;
		this->kids = kids;
		this->sibs = sibs;
		this->prev = prev;
	}

	/** Deep copy constructor. */
	Tree(Tree* root) {
		if (root->kids != NULL) {                     
	    	this->kids = new Tree(root->kids);
	  	} else {
	    	this->kids = NULL;
	  	}
	  	if (root->sibs != NULL) {
	  		this->sibs = new Tree(root->sibs);
	  	} else {
	  		this->sibs = NULL;
	  	}
	}

	/** Destructor. Clears children and siblings to the right of this. */
	~Tree() {
		delete this->kids;
		delete this->sibs;
	}

	/** 
	 * Adds a child to this node. Siblings and children must be unique. This 
	 * returns true if added, and false otherwise.
	 */
	bool addChild(T data) {
		if (this->kids != NULL) {
			return this->kids->addSibling(data);
		} else {
		    Tree* temp = new Tree(data);
		    temp->prev = this;
		    this->kids = temp;
		    return true;
		}
	}

	/** 
	 * Adds a child to this node. Siblings and children must be unique. This 
	 * returns true if added, and false otherwise. The root that is added must
	 * be valid.
	 */
	bool addChild(Tree* root) {
		if (root->prev == NULL && root->sibs == NULL) {
			if (this->kids != NULL) {
				return this->kids->addSibling(root);
			} else {
				root->prev = this;
				this->kids = root;
				return true;
			}
		} else {
			return false;
		}
	}

	/** Removes the child containing data and all of its children. */
	bool removeChild(T data) {

		Tree *curr = this->kids;

		while (curr != NULL) {

			if (curr->data == data) {
				if (curr == this->kids) {
					curr->sibs->prev = curr->prev;
					curr->prev->kids = curr->sibs;
				} else {
					curr->prev->sibs = curr->sibs;
				}

				curr->sibs = NULL;
				delete curr;
				return true;
			} else {
				curr = curr->sibs;
			}
		}

		delete curr;
		return false;

	}

	/** All characters, separated by newlines, including at the end. */
	std::string toString() {
		std::ostringstream oss;
		return toString(this, oss);
	}

	/** Helper method for toString(). */
	std::string toString(Tree* curr, std::ostringstream& oss) {
		oss << curr->data << std::endl;
		if (curr->kids != NULL) {
			toString(curr->kids, oss);
		}
		if (curr->sibs != NULL) {
			toString(curr->sibs, oss);
		}
		return oss.str();
	}

private:

	/**
	 * Iterate through the list of siblings and check if data is equal to any of them.
	 * If data is equal to any of them, return false because we cannot have duplicate siblings
	 * If we reach the end of the list and no matches are found, we add data to the list.
	 */
	bool addSibling(T data) {

		if (this->prev == NULL) {
			return false;
		}
		
		Tree* curr = this;

		while (curr->sibs != NULL && curr->sibs->data < data) {
			curr = curr->sibs;
		}

		if ((curr->sibs != NULL && curr->sibs->data == data) || (curr->data == data)) {
			return false;
		}

		Tree* t = new Tree(data);

		if (curr->data < data) {
			t->sibs = curr->sibs;
			t->prev = curr;
			curr->sibs = t;
			return true;
		}

		t->sibs = curr;
		t->prev = curr->prev;
		curr->prev->kids = t;
		curr->prev = t;
		return true;
	}

	/**
	 * This function returns false is the root is invalid (has prev or siblings), 
	 * or if the data in the root node is already a child of the tree node. 
	 * Otherwise it adds the node in order and returns true.
	 */
	bool addSibling(Tree *root) {

		if (this->prev == NULL) {
			return false;
		}

		Tree* curr = this;

		while (curr->sibs != NULL && curr->sibs->data < root->data) {
			curr = curr->sibs;
		}

		if ((curr->sibs != NULL && curr->sibs->data == root->data) || (curr->data == root->data)) {
			return false;
		}

		if (curr->data < root->data) {
			root->sibs = curr->sibs;
			root->prev = curr;
			curr->sibs = root;
			return true;
		} 

		root->sibs = curr;
		root->prev = curr->prev;
		curr->prev->kids = root;
		curr->prev = root;
		return true;
	}

};

#endif
