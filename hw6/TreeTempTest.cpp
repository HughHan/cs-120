/**
 *  Hugh Han
 *  EN.600.120 Intermediate Programming, Spring 2015
 *  HW 6C: Tree<char>Test.cpp
 *  
 *  April 13, 2015
 *  Phone: (516) 707-1344
 *  JHED ID: hhan17
 *  JHU Email: hhan17@jhu.edu
 */

#include "Tree.hpp"
#include "FileDir.h"
#include <string>
#include <cassert>
#include <iostream>

using std::cout;
using std::endl;
using std::string;

class TreeTempTest {
public:

    /**
     *  This tests constructing a Tree<char>.
     */
    static void constructorTestChar() {
        Tree<char> t1('A');
        assert(t1.toString() == "A\n");
        Tree<char> t2('b');
        assert(t2.toString() == "b\n");
        Tree<char> t3('^');
        assert(t3.toString() == "^\n");
    }

    /**
     *  This tests constructing a Tree<int>.
     */
    static void constructorTestInt() {
        Tree<int> t4(2147483647);
        assert(t4.toString() == "2147483647\n");
        Tree<int> t5(1);
        assert(t5.toString() == "1\n");
        Tree<int> t6(0);
        assert(t6.toString() == "0\n");
        Tree<int> t7(-1);
        assert(t7.toString() == "-1\n");
        Tree<int> t8(-2147483647);
        assert(t8.toString() == "-2147483647\n");
    }

    /**
     *  This tests constructing a Tree<std::string>.
     */
    static void constructorTestString() {
        Tree<string> t9("");
        assert(t9.toString() == "\n");
        Tree<string> t10("\n");
        assert(t10.toString() == "\n\n");
        Tree<string> t11("c++ is a Co0L L@nGuAGe!");
        assert(t11.toString() == "c++ is a Co0L L@nGuAGe!\n");
        Tree<string> t12("aj83ianp8&#&9dha@hd");
        assert(t12.toString() == "aj83ianp8&#&9dha@hd\n");
    }

    /**
     *  This tests constructing a Tree<FileDir>.
     */
    static void constructorTestFileDir() {

        Tree<FileDir> t1(*(new FileDir("")));
        assert(t1.toString() == " [4kb]\n");
        Tree<FileDir> t2(*(new FileDir("\n", 3293842499248294, false)));
        assert(t2.toString() == "\n [3293842499248294kb]\n");
        Tree<FileDir> t3(*(new FileDir("c++ is a Co0L L@nGuAGe!")));
        assert(t3.toString() == "c++ is a Co0L L@nGuAGe! [4kb]\n");
        Tree<FileDir> t4(*(new FileDir("aj83ianp8&#&9dha@hd", 70, true)));
        assert(t4.toString() == "aj83ianp8&#&9dha@hd/ [70kb]\n");
    
    }

    /**
     *  This tests constructing a Tree<char> by copying values from another Tree<char>.
     */
    static void copyConstructorTestChar() {
        // A
        Tree<char>* t1Ptr = new Tree<char>('A');
        Tree<char>& t1 = *t1Ptr; // avoids a bunch of *'s below
        assert(t1.toString() == "A\n");

        // A
        // |
        // b
        assert(t1.addChild('b'));
        assert(t1.toString() == "A\nb\n");
        // can't add again
        assert(!t1.addChild('b'));
        assert(t1.toString() == "A\nb\n");

        // A
        // |
        // b - c
        assert(t1.addChild('c'));
        assert(t1.toString() == "A\nb\nc\n");
        // can't add again
        assert(!t1.addChild('c'));
        assert(t1.toString() == "A\nb\nc\n");

        Tree<char> t2(t1);
        assert(t2.toString() == "A\nb\nc\n");

    }

    /**
     *  This tests constructing a Tree<int> by copying values from another Tree<int>.
     */
    static void copyConstructorTestInt() {
        // 1
        Tree<int>* t1Ptr = new Tree<int>(1);
        Tree<int>& t1 = *t1Ptr; // avoids a bunch of *'s below
        assert(t1.toString() == "1\n");

        // 1
        // |
        // 0
        assert(t1.addChild(0));
        assert(t1.toString() == "1\n0\n");
        // can't add again
        assert(!t1.addChild(0));
        assert(t1.toString() == "1\n0\n");

        // 1
        // |
        // -2147483647 - 0
        assert(t1.addChild(-2147483647));
        assert(t1.toString() == "1\n-2147483647\n0\n");
        // can't add again
        assert(!t1.addChild(-2147483647));
        assert(t1.toString() == "1\n-2147483647\n0\n");

        Tree<int> t2(t1);
        assert(t2.toString() == "1\n-2147483647\n0\n");
    }

    /**
     *  This tests constructing a Tree<std::string> by copying values from another 
     *  Tree<std::string>.
     */
    static void copyConstructorTestString() {
        // 
        Tree<string>* t1Ptr = new Tree<string>("");
        Tree<string>& t1 = *t1Ptr; // avoids a bunch of *'s below
        assert(t1.toString() == "\n");

        // 
        // |
        // \n
        assert(t1.addChild("\n"));
        assert(t1.toString() == "\n\n\n");
        // can't add again
        assert(!t1.addChild("\n"));
        assert(t1.toString() == "\n\n\n");

        // 
        // |
        // \n - c++ is a Co0L L@nGuAGe!
        assert(t1.addChild("c++ is a Co0L L@nGuAGe!"));
        assert(t1.toString() == "\n\n\nc++ is a Co0L L@nGuAGe!\n");
        // can't add again
        assert(!t1.addChild("c++ is a Co0L L@nGuAGe!"));
        assert(t1.toString() == "\n\n\nc++ is a Co0L L@nGuAGe!\n");

        Tree<string> t2(t1);
        assert(t2.toString() == "\n\n\nc++ is a Co0L L@nGuAGe!\n");
    }

    /**
     *  This tests constructing a Tree<FileDir> by copying values from another Tree<FileDir>.
     */
    static void copyConstructorTestFileDir() {

        /**
         *  Due to the large number of instance variables that each FileDir has, the trees were
         *  not drawn in comments as a part of this code. I tried at first, and it became too
         *  difficult and tedious to keep track of everything in the text editor. I made sure to
         *  test that everything is added in the correct order, with directories first and files
         *  last. Furthermore, it is in alphabetical order.
         */

        // Construct
        Tree<FileDir>* t1Ptr = new Tree<FileDir>(*(new FileDir("new FileDir")));
        Tree<FileDir>& t1 = *t1Ptr; // avoids a bunch of *'s below
        assert(t1.toString() == "new FileDir [4kb]\n");

        // Add
        assert(t1.addChild(*(new FileDir("swaggie FileDir", 7, true))));
        assert(t1.toString() == "new FileDir [4kb]\nswaggie FileDir/ [7kb]\n");

        // can't add again
        assert(!t1.addChild(*(new FileDir("swaggie FileDir", 7, true))));
        assert(t1.toString() == "new FileDir [4kb]\nswaggie FileDir/ [7kb]\n");

        // Add
        assert(t1.addChild(*(new FileDir("C++ is a Co0L L@nGuAGe!"))));
        assert(t1.toString() == "new FileDir [4kb]\nswaggie FileDir/ [7kb]\nC++ is a Co0L L@nGuAGe! [4kb]\n");

        // can't add again
        assert(!t1.addChild(*(new FileDir("C++ is a Co0L L@nGuAGe!"))));
        assert(t1.toString() == "new FileDir [4kb]\nswaggie FileDir/ [7kb]\nC++ is a Co0L L@nGuAGe! [4kb]\n");

        // Add (Note that c++ is now lower case and a directory)
        assert(t1.addChild(*(new FileDir("c++ is a Co0L L@nGuAGe!", 2, true))));
        assert(t1.toString() == "new FileDir [4kb]\nc++ is a Co0L L@nGuAGe!/ [2kb]\nswaggie FileDir/ [7kb]\nC++ is a Co0L L@nGuAGe! [4kb]\n");

        // can't add again
        assert(!t1.addChild(*(new FileDir("c++ is a Co0L L@nGuAGe!", 2, true))));
        assert(t1.toString() == "new FileDir [4kb]\nc++ is a Co0L L@nGuAGe!/ [2kb]\nswaggie FileDir/ [7kb]\nC++ is a Co0L L@nGuAGe! [4kb]\n");

        // Copy
        Tree<FileDir> t2(t1);
        assert(t2.toString() == "new FileDir [4kb]\nc++ is a Co0L L@nGuAGe!/ [2kb]\nswaggie FileDir/ [7kb]\nC++ is a Co0L L@nGuAGe! [4kb]\n");
    }

    /**
     *  This tests adding a type char to a Tree<char>.
     */
    static void addsTestChar() {
        // A
        Tree<char>* t1Ptr = new Tree<char>('A');
        Tree<char>& t1 = *t1Ptr; // avoids a bunch of *'s below
        assert(t1.toString() == "A\n");

        // A
        // |
        // b
        assert(t1.addChild('b'));
        assert(t1.toString() == "A\nb\n");
        // can't add again
        assert(!t1.addChild('b'));
        assert(t1.toString() == "A\nb\n");

        // A
        // |
        // b - c
        assert(t1.addChild('c'));
        assert(t1.toString() == "A\nb\nc\n");
        // can't add again
        assert(!t1.addChild('c'));
        assert(t1.toString() == "A\nb\nc\n");

        // A
        // |
        // B - b - c
        assert(t1.addChild('B'));
        // 'B' comes before 'b'
        assert(t1.toString() == "A\nB\nb\nc\n");
        // can't add repeats
        assert(!t1.addChild('B'));
        assert(!t1.addChild('b'));
        assert(!t1.addChild('c'));
        assert(t1.toString() == "A\nB\nb\nc\n");

        // can't add 'A' as sibling of 'A'
        assert(!t1.addSibling('A'));
        assert(t1.toString() == "A\nB\nb\nc\n");

        // make sure that we can't add siblings to the root
        assert(!t1.addSibling('C'));
        assert(t1.toString() == "A\nB\nb\nc\n");

        // Adding in an already built subTree<char>
        // First build another Tree<char>
        // R
        Tree<char>* t2Ptr = new Tree<char>('R');
        Tree<char>& t2 = *t2Ptr;
        assert(t2.toString() == "R\n");
        
        // R
        // |
        // C
        assert(t2.addChild('C'));
        assert(t2.toString() == "R\nC\n");

        // R
        // |
        // C - d
        assert(t2.addChild('d'));
        assert(t2.toString() == "R\nC\nd\n");
        // can't repeat
        assert(!t2.addChild('d'));
        assert(t2.toString() == "R\nC\nd\n");

        // R
        // |
        // B - C - d
        assert(t2.addChild('B'));
        assert(t2.toString() == "R\nB\nC\nd\n");
        // can't repeat
        assert(!t2.addChild('B'));
        assert(t2.toString() == "R\nB\nC\nd\n");

        // Add t1 in as a child
        // R
        // |
        // A - B - C - d
        // |
        // B - b - c

        // t1 is as before
        assert(t1.toString() == "A\nB\nb\nc\n");
        // add t1 to t2
        assert(t2.addChild(&t1));
        // t1 should now have siblings
        assert(t1.toString() == "A\nB\nb\nc\nB\nC\nd\n");
        // t2 should be updated
        assert(t2.toString() == "R\nA\nB\nb\nc\nB\nC\nd\n");

        // R
        // |
        // @ - A - B - C - d
        //     |
        //     B - b - c
        assert(t2.addChild('@'));
        assert(t2.toString() == "R\n@\nA\nB\nb\nc\nB\nC\nd\n");
        // shouldn't be able to add duplicate children
        assert(!t2.addChild('@'));
        assert(!t2.addChild('A'));
        assert(!t2.addChild('B'));
        assert(!t2.addChild('C'));
        assert(!t2.addChild('d'));

        // R
        // |
        // @ - A - B - C - D - d
        //     |
        //     B - b - c
        assert(t2.addChild('D'));
        assert(t2.toString() == "R\n@\nA\nB\nb\nc\nB\nC\nD\nd\n");

        // R
        // |
        // @ - A - B - C - D - d - e
        //     |
        //     B - b - c
        assert(t2.addChild('e'));
        assert(t2.toString() == "R\n@\nA\nB\nb\nc\nB\nC\nD\nd\ne\n");

        delete t2Ptr;
        
    }

    /**
     *  This tests adding a type int to a Tree<int>.
     */
    static void addsTestInt() {
        // 1
        Tree<int>* t1Ptr = new Tree<int>(1);
        Tree<int>& t1 = *t1Ptr; // avoids a bunch of *'s below
        assert(t1.toString() == "1\n");

        // 1
        // |
        // 0
        assert(t1.addChild(0));
        assert(t1.toString() == "1\n0\n");
        // can't add again
        assert(!t1.addChild(0));
        assert(t1.toString() == "1\n0\n");

        // 1
        // |
        // -2147483647 - 0
        assert(t1.addChild(-2147483647));
        assert(t1.toString() == "1\n-2147483647\n0\n");
        // can't add again
        assert(!t1.addChild(-2147483647));
        assert(t1.toString() == "1\n-2147483647\n0\n");

        // 1
        // |
        // -2147483647 - 0 - 2147483647
        assert(t1.addChild(2147483647));
        assert(t1.toString() == "1\n-2147483647\n0\n2147483647\n");
        // can't add repeats
        assert(!t1.addChild(0));
        assert(!t1.addChild(-2147483647));
        assert(!t1.addChild(2147483647));
        assert(t1.toString() == "1\n-2147483647\n0\n2147483647\n");

        // can't add 1 as sibling of 1
        assert(!t1.addSibling(1));
        assert(t1.toString() == "1\n-2147483647\n0\n2147483647\n");

        // make sure that we can't add siblings to the root
        assert(!t1.addSibling(3));
        assert(t1.toString() == "1\n-2147483647\n0\n2147483647\n");

        // Adding in an already built subTree<int>
        // First build another Tree<int>
        // 7
        Tree<int>* t2Ptr = new Tree<int>(7);
        Tree<int>& t2 = *t2Ptr;
        assert(t2.toString() == "7\n");
        
        // 7
        // |
        // 8
        assert(t2.addChild(8));
        assert(t2.toString() == "7\n8\n");

        // 7
        // |
        // 8 - 9
        assert(t2.addChild(9));
        assert(t2.toString() == "7\n8\n9\n");
        // can't repeat
        assert(!t2.addChild(9));
        assert(t2.toString() == "7\n8\n9\n");

        // 7
        // |
        // 8 - 9 - 11
        assert(t2.addChild(11));
        assert(t2.toString() == "7\n8\n9\n11\n");
        // can't repeat
        assert(!t2.addChild(11));
        assert(t2.toString() == "7\n8\n9\n11\n");

        // Add t1 in as a child
        // 7
        // |
        // 1 - 8 - 9 - 11
        // |
        // -2147483647 - 0 - 2147483647

        // t1 is as before
        assert(t1.toString() == "1\n-2147483647\n0\n2147483647\n");
        // add t1 to t2
        assert(t2.addChild(&t1));
        // t1 should now have siblings
        assert(t1.toString() == "1\n-2147483647\n0\n2147483647\n8\n9\n11\n");
        // t2 should be updated
        assert(t2.toString() == "7\n1\n-2147483647\n0\n2147483647\n8\n9\n11\n");

        // 7
        // |
        // 0 - 1 - 8 - 9 - 11
        //     |
        //     -2147483647 - 0 - 2147483647
        assert(t2.addChild(0));
        assert(t2.toString() == "7\n0\n1\n-2147483647\n0\n2147483647\n8\n9\n11\n");
        // shouldn't be able to add duplicate children
        assert(!t2.addChild(0));
        assert(!t2.addChild(1));
        assert(!t2.addChild(8));
        assert(!t2.addChild(9));
        assert(!t2.addChild(11));

        // 7
        // |
        // 0 - 1 - 8 - 9 - 10 - 11
        //     |
        //     -2147483647 - 0 - 2147483647
        assert(t2.addChild(10));
        assert(t2.toString() == "7\n0\n1\n-2147483647\n0\n2147483647\n8\n9\n10\n11\n");

        // 7
        // |
        // 0 - 1 - 8 - 9 - 10 - 11 - 12
        //     |
        //     -2147483647 - 0 - 2147483647
        assert(t2.addChild(12));
        assert(t2.toString() == "7\n0\n1\n-2147483647\n0\n2147483647\n8\n9\n10\n11\n12\n");

        delete t2Ptr;
       
    }

    /**
     *  This tests adding a type std::string to a Tree<std::string>.
     */
    static void addsTestString() {
        // 1
        Tree<string>* t1Ptr = new Tree<string>("1");
        Tree<string>& t1 = *t1Ptr; // avoids a bunch of *'s below
        assert(t1.toString() == "1\n");

        // 1
        // |
        // 0
        assert(t1.addChild("0"));
        assert(t1.toString() == "1\n0\n");
        // can't add again
        assert(!t1.addChild("0"));
        assert(t1.toString() == "1\n0\n");

        // 1
        // |
        // -2147483647 - 0
        assert(t1.addChild("-2147483647"));
        assert(t1.toString() == "1\n-2147483647\n0\n");
        // can't add again
        assert(!t1.addChild("-2147483647"));
        assert(t1.toString() == "1\n-2147483647\n0\n");

        // 1
        // |
        // -2147483647 - 0 - 2147483647
        assert(t1.addChild("2147483647"));
        assert(t1.toString() == "1\n-2147483647\n0\n2147483647\n");
        // can't add repeats
        assert(!t1.addChild("0"));
        assert(!t1.addChild("-2147483647"));
        assert(!t1.addChild("2147483647"));
        assert(t1.toString() == "1\n-2147483647\n0\n2147483647\n");

        // can't add 1 as sibling of 1
        assert(!t1.addSibling("1"));
        assert(t1.toString() == "1\n-2147483647\n0\n2147483647\n");

        // make sure that we can't add siblings to the root
        assert(!t1.addSibling("3"));
        assert(t1.toString() == "1\n-2147483647\n0\n2147483647\n");

        // Adding in an already built subTree<int>
        // First build another Tree<int>
        // 7
        Tree<string>* t2Ptr = new Tree<string>("7");
        Tree<string>& t2 = *t2Ptr;
        assert(t2.toString() == "7\n");
        
        // 7
        // |
        // 8
        assert(t2.addChild("8"));
        assert(t2.toString() == "7\n8\n");

        // 7
        // |
        // 8 - 9
        assert(t2.addChild("9"));
        assert(t2.toString() == "7\n8\n9\n");
        // can't repeat
        assert(!t2.addChild("9"));
        assert(t2.toString() == "7\n8\n9\n");

        // 7
        // |
        // 11 - 8 - 9
        assert(t2.addChild("11"));
        assert(t2.toString() == "7\n11\n8\n9\n");
        // can't repeat
        assert(!t2.addChild("11"));
        assert(t2.toString() == "7\n11\n8\n9\n");

        // Add t1 in as a child
        // 7
        // |
        // 1 - 11 - 8 - 9
        // |
        // -2147483647 - 0 - 2147483647

        // t1 is as before
        assert(t1.toString() == "1\n-2147483647\n0\n2147483647\n");
        // add t1 to t2
        assert(t2.addChild(&t1));
        // t1 should now have siblings
        assert(t1.toString() == "1\n-2147483647\n0\n2147483647\n11\n8\n9\n");
        // t2 should be updated
        assert(t2.toString() == "7\n1\n-2147483647\n0\n2147483647\n11\n8\n9\n");

        // 7
        // |
        // 0 - 1 - 11 - 8 - 9
        //     |
        //     -2147483647 - 0 - 2147483647
        assert(t2.addChild("0"));
        assert(t2.toString() == "7\n0\n1\n-2147483647\n0\n2147483647\n11\n8\n9\n");
        // shouldn't be able to add duplicate children
        assert(!t2.addChild("0"));
        assert(!t2.addChild("1"));
        assert(!t2.addChild("8"));
        assert(!t2.addChild("9"));
        assert(!t2.addChild("11"));

        // 7
        // |
        // 0 - 1 - 10 - 11 - 8 - 9
        //     |
        //     -2147483647 - 0 - 2147483647
        assert(t2.addChild("10"));
        assert(t2.toString() == "7\n0\n1\n-2147483647\n0\n2147483647\n10\n11\n8\n9\n");

        // 7
        // |
        // 0 - 1 - 10 - 11 - 12 - 8 - 9
        //     |
        //     -2147483647 - 0 - 2147483647
        assert(t2.addChild("12"));
        assert(t2.toString() == "7\n0\n1\n-2147483647\n0\n2147483647\n10\n11\n12\n8\n9\n");

        delete t2Ptr;
       
    }

    /**
     *  This tests adding a type FileDir to a Tree<FileDir>.
     */
    static void addsTestFileDir() {
        
        /**
         *  Due to the large number of instance variables that each FileDir has, the trees were
         *  not drawn in comments as a part of this code. I tried at first, and it became too
         *  difficult and tedious to keep track of everything in the text editor. I made sure to
         *  test that everything is added in the correct order, with directories first and files
         *  last. Furthermore, it is in alphabetical order.
         */

        // Construct
        Tree<FileDir>* t1Ptr = new Tree<FileDir>(*(new FileDir("new FileDir")));
        Tree<FileDir>& t1 = *t1Ptr; // avoids a bunch of *'s below
        assert(t1.toString() == "new FileDir [4kb]\n");

        // Add
        assert(t1.addChild(*(new FileDir("swaggie FileDir"))));
        assert(t1.toString() == "new FileDir [4kb]\nswaggie FileDir [4kb]\n");
        // can't add again
        assert(!t1.addChild(*(new FileDir("swaggie FileDir"))));
        assert(t1.toString() == "new FileDir [4kb]\nswaggie FileDir [4kb]\n");

        // Add
        assert(t1.addChild(*(new FileDir("Swaggie FileDir"))));
        assert(t1.toString() == "new FileDir [4kb]\nSwaggie FileDir [4kb]\nswaggie FileDir [4kb]\n");
        // can't add again
        assert(!t1.addChild(*(new FileDir("Swaggie FileDir"))));
        assert(t1.toString() == "new FileDir [4kb]\nSwaggie FileDir [4kb]\nswaggie FileDir [4kb]\n");

        // Add
        assert(t1.addChild(*(new FileDir("zee first Dir", 5, true))));

        assert(t1.toString() == "new FileDir [4kb]\nzee first Dir/ [5kb]\nSwaggie FileDir [4kb]\nswaggie FileDir [4kb]\n");
        // can't add repeats
        assert(!t1.addChild(*(new FileDir("zee first Dir", 5, true))));
        assert(!t1.addChild(*(new FileDir("Swaggie FileDir"))));
        assert(t1.toString() == "new FileDir [4kb]\nzee first Dir/ [5kb]\nSwaggie FileDir [4kb]\nswaggie FileDir [4kb]\n");

        // can't add new FileDir as sibling of new FileDir
        assert(!t1.addSibling(*(new FileDir("new FileDir"))));
        assert(t1.toString() == "new FileDir [4kb]\nzee first Dir/ [5kb]\nSwaggie FileDir [4kb]\nswaggie FileDir [4kb]\n");

        // make sure that we can't add siblings to the root
        assert(!t1.addSibling(*(new FileDir("super FileDir", 7, true))));
        assert(t1.toString() == "new FileDir [4kb]\nzee first Dir/ [5kb]\nSwaggie FileDir [4kb]\nswaggie FileDir [4kb]\n");

        // Adding in an already built subTree<FileDir>
        // First build another Tree<FileDir>
        Tree<FileDir>* t2Ptr = new Tree<FileDir>(*(new FileDir("HOOPLA!", 70, true)));
        Tree<FileDir>& t2 = *t2Ptr;
        assert(t2.toString() == "HOOPLA!/ [70kb]\n");
        
        // Add
        assert(t2.addChild(*(new FileDir("DOOPLA!", 90, true))));
        assert(t2.toString() == "HOOPLA!/ [70kb]\nDOOPLA!/ [90kb]\n");

        // Add
        assert(t2.addChild(*(new FileDir("oopla?", 90, true))));
        assert(t2.toString() == "HOOPLA!/ [70kb]\nDOOPLA!/ [90kb]\noopla?/ [90kb]\n");
        // can't repeat
        assert(!t2.addChild(*(new FileDir("oopla?", 90, true))));
        assert(t2.toString() == "HOOPLA!/ [70kb]\nDOOPLA!/ [90kb]\noopla?/ [90kb]\n");

        // Add
        assert(t2.addChild(*(new FileDir("duper file :)", 3, false))));
        assert(t2.toString() == "HOOPLA!/ [70kb]\nDOOPLA!/ [90kb]\noopla?/ [90kb]\nduper file :) [3kb]\n");
        // can't repeat
        assert(!t2.addChild(*(new FileDir("duper file :)", 3, false))));
        assert(t2.toString() == "HOOPLA!/ [70kb]\nDOOPLA!/ [90kb]\noopla?/ [90kb]\nduper file :) [3kb]\n");

        // Add t1 in as a child

        // t1 is as before
        assert(t1.toString() == "new FileDir [4kb]\nzee first Dir/ [5kb]\nSwaggie FileDir [4kb]\nswaggie FileDir [4kb]\n");

        // add t1 to t2
        assert(t2.addChild(&t1));

        // t1 should now have siblings
        assert(t1.toString() == "new FileDir [4kb]\nzee first Dir/ [5kb]\nSwaggie FileDir [4kb]\nswaggie FileDir [4kb]\n");

        // t2 should be updated
        assert(t2.toString() == "HOOPLA!/ [70kb]\nDOOPLA!/ [90kb]\noopla?/ [90kb]\nduper file :) [3kb]\nnew FileDir [4kb]\nzee first Dir/ [5kb]\nSwaggie FileDir [4kb]\nswaggie FileDir [4kb]\n");

        delete t2Ptr;      
    }

    /**
     *  This test was from the previous part (Part B), and tests adding a single child. For the 
     *  purposes of Part C, this is pretty insignificant and you can pretty much ignore this.
     */
    static void addSimpleChildTest() {
        // A
        Tree<char>* t1 = new Tree<char>('A');
        assert(t1->toString() == "A\n");

        // A
        Tree<char>* t2 = new Tree<char>('A');
        assert(t2->toString() == "A\n");

        // A
        // |
        // A
        assert(t2->addChild(t1));
        assert(t2->toString() == "A\nA\n");

        delete t2;
    }

    /**
     *  This test was from the previous part (Part B), and tests removing a child. For the 
     *  purposes of Part C, this is pretty insignificant and you can pretty much ignore this.
     */
    static void removeTest() {

        // A
        // |
        // B - b - c
        Tree<char>* t1Ptr = new Tree<char>('A');
        Tree<char>& t1 = *t1Ptr; // avoids a bunch of *'s below
        assert(t1.toString() == "A\n");
        assert(t1.addChild('b'));
        assert(t1.addChild('c'));
        assert(t1.addChild('B'));

        // R
        // |
        // A - B - C - d
        // |
        // B - b - c
        Tree<char>* t2Ptr = new Tree<char>('R');
        Tree<char>& t2 = *t2Ptr;
        assert(t2.toString() == "R\n");
        assert(t2.addChild('C'));
        assert(t2.addChild('d'));
        assert(t2.addChild('B'));
        assert(t2.addChild(&t1));
        
        // D is not in the root
        assert(!t2.removeChild('D'));

        // B is not in the root
        assert(t2.removeChild('B'));

        // remove d in the second level
        assert(t2.removeChild('d'));

        // b is not in the second level
        assert(!t2.removeChild('b'));

        // remove b in the third level
        assert(t2.kids->removeChild('b'));

        // remove c in the third level
        assert(t2.kids->removeChild('c'));

    }

    /**
     *  This tests FileDir functions.
     */
    static void fileDirTest() {

        // Construct
        FileDir* fileDirPtr = new FileDir("Test", 1, true);
        FileDir& fileDir = *fileDirPtr;

        // Accessors
        assert(fileDir.getName() == "Test");
        assert(fileDir.getSize() == 1);
        assert(!fileDir.isFile());

        // Mutators
        assert(fileDir.rename("New Test") == "Test");
        assert(fileDir.getName() == "New Test");
        assert(fileDir.resize(-10000) == 1);
        assert(fileDir.resize(3) == 4);
        assert(fileDir.resize(-4) == 0);
        assert(fileDir.getSize() == 0);
    }

    /** 
     *  This tests the overloaded operators.
     */
    static void operatorTest() {
        FileDir* f1 = new FileDir("new FileDir");
        FileDir* f2 = new FileDir("swaggie FileDir");
        FileDir* f3 = new FileDir("Swaggie FileDir");
        
        assert(*f1 < *f2);
        assert(!(*f3 < *f2));
        assert(!(*f3 == *f2));

        /**
         *  Please note that below, assert statements were not used. I was not sure how
         *  to test this, so I used the << operator to put the FileDir variables into cout.
         */

        cout << *f1 << endl;
        cout << *f2 << endl;
        cout << *f3 << endl;
    }

};

int main(void) {

    cout << "Testing Tree" << endl << endl;

    /************ CONSTRUCTORS ************/
    cout << "Testing constructors..." << endl;
    cout << "->constructorTestChar()" << endl;
    TreeTempTest::constructorTestChar();
    cout << "->constructorTestInt()" << endl;
    TreeTempTest::constructorTestInt();
    cout << "->constructorTestString()" << endl;
    TreeTempTest::constructorTestString();
    cout << "->constructorTestFileDir()" << endl;
    TreeTempTest::constructorTestFileDir();
    cout << "Constructor tests passed." << endl << endl;

    /********** COPY CONSTRUCTOR **********/
    cout << "Testing copy constructors" << endl;
    cout << "->copyConstructorTestChar()" << endl;
    TreeTempTest::copyConstructorTestChar();
    cout << "->copyConstructorTestInt()" << endl;
    TreeTempTest::copyConstructorTestInt();
    cout << "->copyConstructorTestString()" << endl;
    TreeTempTest::copyConstructorTestString();
    cout << "->copyConstructorTestFileDir()" << endl;
    TreeTempTest::copyConstructorTestFileDir();
    cout << "Copy constructor tests passed." << endl << endl;

    /**************** ADD ****************/
    cout << "Testing add" << endl;
    cout << "->addsTestChar()" << endl;
    TreeTempTest::addsTestChar();
    cout << "->addsTestInt()" << endl;
    TreeTempTest::addsTestInt();
    cout << "->addsTestString()" << endl;
    TreeTempTest::addsTestString();
    cout << "->addsTestFileDir()" << endl;
    TreeTempTest::addsTestFileDir();
    cout << "->addSimpleChildTest()" << endl;
    TreeTempTest::addSimpleChildTest();
    cout << "Add tests passed." << endl << endl;

    /*************** OTHER ***************/
    cout << "Testing others..." << endl;
    cout << "->removeTest()" << endl;
    TreeTempTest::removeTest();
    cout << "->fileDirTest()" << endl;
    TreeTempTest::fileDirTest();
    cout << "->operatorTest()" << endl;
    TreeTempTest::operatorTest();
    cout << "Other tests passed." << endl << endl;

    cout << "Tree tests passed" << endl;
    
}
