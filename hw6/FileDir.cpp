/**
 *  Hugh Han
 *  EN.600.120 Intermediate Programming, Spring 2015
 *  HW 6: FileDir.cpp
 *  
 *	April 13, 2015
 *  Phone: (516) 707-1344
 *  JHED ID: hhan17
 *  JHU Email: hhan17@jhu.edu
 */

#include "FileDir.h"

FileDir::FileDir() {
	this->name = "";
	this->size = 4;
	this->isDir = false;
}

FileDir::FileDir(std::string s, long l, bool b) {
	this->name = s;
	this->size = l;
	this->isDir = b;
}

FileDir::FileDir(FileDir& f) {
	this->name = f.name;
	this->size = f.size;
	this->isDir = f.isDir;
}

std::string FileDir::getName() const {
	return this->name;
}

long FileDir::getSize() const {
	return this->size;
}

bool FileDir::isFile() const {
	return !isDir;
}

std::string FileDir::rename(std::string s) {
	std::string temp = this->name;
	this->name = s;
	return temp;
}

long FileDir::resize(long l) {
	if (this->size + l >= 0)
		this->size += l;
	return this->size;
}

std::string FileDir::toString() {
	std::string s = this->name;
	if (this->isDir) 
		s += "/";
	std::ostringstream oss;
	oss << s << " [" << this->size << "kb]";
	return oss.str();
}

bool FileDir::operator==(const FileDir& right) {

	// check their type and their names
	return (this->isFile() == right.isFile()) && (this->name == right.name);
}

bool FileDir::operator<(const FileDir& right) {

	// check if they are of the same type
	if (this->isFile()) {
		if (!right.isFile()) {
			return false;
		}
	} else {
		if (right.isFile()){
			return true;
		}
	}

	// if they are, then compare their names
	std::string l = this->name;
	std::string r = right.name;

	for (unsigned long i = 0; i < this->name.length(); i++) {
    	l[i] = tolower(this->name[i]);
	}
	for (unsigned long i = 0; i < right.name.length(); i++) {
    	r[i] = tolower(right.name[i]);
	}
	return l.compare(r) < 0;
	
}

std::ostream& operator<<(std::ostream& os, const FileDir& fileDir) {
    os << fileDir.getName();
    if (!fileDir.isFile()) 
		os << "/";
	os << " [" << fileDir.getSize() << "kb]";
    return os;
}
