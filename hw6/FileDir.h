/**
 *  Hugh Han
 *  EN.600.120 Intermediate Programming, Spring 2015
 *  HW 6: FileDir.h
 *  
 *	April 13, 2015
 *  Phone: (516) 707-1344
 *  JHED ID: hhan17
 *  JHU Email: hhan17@jhu.edu
 */

#ifndef _FILEDIR_H
#define _FILEDIR_H

#include <cctype>
#include <string>
#include <sstream>
#include <iostream>

class FileDir {

private:
	
	std::string name;
	long size;
	bool isDir;

public:

	FileDir();
	FileDir(std::string s, long l = 4, bool b = false);
	FileDir(FileDir& f);

	std::string getName() const;
	long getSize() const;
	bool isFile() const;

	std::string rename(std::string s);
	long resize(long l);

	std::string toString();

	bool operator==(const FileDir& right);
	bool operator<(const FileDir& right);
	friend std::ostream& operator<<(std::ostream& os, const FileDir& fileDir);

};

#endif
