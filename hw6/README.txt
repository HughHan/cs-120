Hugh Han
600.120 Intermediate Programming, Spring 2015
HW 6C
Phone: (516) 707-1344
JHED ID: hhan17
JHU Email: hhan17@jhu.edu

To compile and run:
	> make

Alternatively:
	> make test
