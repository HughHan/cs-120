/**
 *  Hugh Han
 *  EN.600.120 Intermediate Programming, Spring 2015
 *  HW 6B: CTree.h
 *  
April 10, 2015
 *  Phone: (516) 707-1344
 *  JHED ID: hhan17
 *  JHU Email: hhan17@jhu.edu
 */

#ifndef _CTREE_H
#define _CTREE_H

#include <cstdlib>
#include <string>
#include <sstream>

class CTree {

	friend class CTreeTest;

	char data; 
	CTree* kids; 
	CTree* sibs;
	CTree* prev;

public:

	CTree(char ch, CTree* kids = NULL, CTree* sibs = NULL, CTree* prev = NULL);
	CTree(CTree* root);
	~CTree();

	bool addChild(char ch);
	bool addChild(CTree* root);
	bool removeChild(char ch);

	std::string toString();
	std::string toString(CTree* curr, std::ostringstream& oss);

private:

	bool addSibling(char ch);
	bool addSibling(CTree *root);

};

#endif
