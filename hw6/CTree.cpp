/**
 *  Hugh Han
 *  EN.600.120 Intermediate Programming, Spring 2015
 *  HW 6B: CTree.cpp
 *  
April 10, 2015
 *  Phone: (516) 707-1344
 *  JHED ID: hhan17
 *  JHU Email: hhan17@jhu.edu
 */

#include "CTree.h"

/** Default constructor. */
CTree::CTree(char ch, CTree* kids, CTree* sibs, CTree* prev) {
	this->data = ch;
	this->kids = kids;
	this->sibs = sibs;
	this->prev = prev;
}

CTree::CTree(CTree* root) {
	if (root->kids != NULL) {                     
    	this->kids = new CTree(root->kids);
  	} else {
    	this->kids = NULL;
  	}
  	if (root->sibs != NULL) {
  		this->sibs = new CTree(root->sibs);
  	} else {
  		this->sibs = NULL;
  	}
}

/** clear siblings to right and children and this node */
CTree::~CTree() {
	delete this->kids;
	delete this->sibs;
}

/** siblings and children must be unique, return true if added, false otherwise */
bool CTree::addChild(char ch) {

	if (this->kids != NULL) {
		return this->kids->addSibling(ch);
	} else {
	    CTree* temp = new CTree(ch);
	    temp->prev = this;
	    this->kids = temp;
	    return true;
	}

}

/**
 * Add tree root for better building, root should have null prev and sibs.
 * Returns false on any type of failure, including invalid root.
 */
bool CTree::addChild(CTree* root) {

	if (root->prev == NULL && root->sibs == NULL) {
		if (this->kids != NULL) {
			return this->kids->addSibling(root);
		} else {
			root->prev = this;
			this->kids = root;
			return true;
		}
	} else {
		return false;
	}
}

/** Removes the child containing ch and all of its children. */
bool CTree::removeChild(char ch) {

	CTree *curr = this->kids;

	while (curr != NULL) {

		if (curr->data == ch) {
			if (curr == this->kids) {
				curr->sibs->prev = curr->prev;
				curr->prev->kids = curr->sibs;
			} else {
				curr->prev->sibs = curr->sibs;
			}

			curr->sibs = NULL;
			delete curr;
			return true;
		} else {
			curr = curr->sibs;
		}
	}

	delete curr;
	return false;

}

/** All characters, separated by newlines, including at the end. */
std::string CTree::toString() {
	std::ostringstream oss;
	return toString(this, oss);
}

/** Helper method for toString(). */
std::string CTree::toString(CTree* curr, std::ostringstream& oss) {
	oss << curr->data << std::endl;
	if (curr->kids != NULL) {
		toString(curr->kids, oss);
	}
	if (curr->sibs != NULL) {
		toString(curr->sibs, oss);
	}
	return oss.str();
}

/**
 * Iterate through the list of siblings and check if ch is equal to any of them.
 * If ch is equal to any of them, return false because we cannot have duplicate siblings
 * If we reach the end of the list and no matches are found, we add ch to the list.
 */
bool CTree::addSibling(char ch) {

	if (this->prev == NULL) {
		return false;
	}
	
	CTree* curr = this;

	while (curr->sibs != NULL && curr->sibs->data < ch) {
		curr = curr->sibs;
	}

	if ((curr->sibs != NULL && curr->sibs->data == ch) || (curr->data == ch)) {
		return false;
	}

	CTree* t = new CTree(ch);

	if (curr->data < ch) {
		t->sibs = curr->sibs;
		t->prev = curr;
		curr->sibs = t;
		return true;
	}

	t->sibs = curr;
	t->prev = curr->prev;
	curr->prev->kids = t;
	curr->prev = t;
	return true;
}

/**
 * This function returns false is the root is invalid (has prev or siblings), 
 * or if the data in the root node is already a child of the tree node. 
 * Otherwise it adds the node in order and returns true.
 */
bool CTree::addSibling(CTree *root) {

	if (this->prev == NULL) {
		return false;
	}

	CTree* curr = this;

	while (curr->sibs != NULL && curr->sibs->data < root->data) {
		curr = curr->sibs;
	}

	if ((curr->sibs != NULL && curr->sibs->data == root->data) || (curr->data == root->data)) {
		return false;
	}

	if (curr->data < root->data) {
		root->sibs = curr->sibs;
		root->prev = curr;
		curr->sibs = root;
		return true;
	} 

	root->sibs = curr;
	root->prev = curr->prev;
	curr->prev->kids = root;
	curr->prev = root;
	return true;
}
