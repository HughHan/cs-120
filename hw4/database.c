/**
 *  Hugh Han
 *  EN.600.120 Intermediate Programming, Spring 2015
 *  HW 4: database.c
 *  
 *  March 10, 2015
 *  Phone: (516) 707-1344
 *  JHED ID: hhan17
 *  JHU Email: hhan17@jhu.edu
 */

#include "hw4.h"

#define	FILENAME_LENGTH 100
#define COURSE_NUMS 800

/* gets the index of a course depending on its department and number */
int get_course_index(int dept, int num) {
	return (COURSE_NUMS*(dept-1)) + (num-100);
}

/* checks if a course exists in the database */
bool course_exists(FILE* datfile, int dept, int num) {

	int index = get_course_index(dept, num);
	Course *cptr = malloc(sizeof(Course));

	fseek(datfile, index * sizeof(Course), SEEK_SET);
	fread(cptr, sizeof(Course), 1, datfile);

	if ((cptr->dept == dept) && (cptr->num == num)) {
		return true;
	} else {
		return false;
	}

	free(cptr);

}

/* creates a database of blank courses */
void create_blank_database(FILE* datfile) {

	Course* blank_course = create_course(BLANK_DIV, BLANK_DEPT, BLANK_NUM, BLANK_CREDITS, BLANK_NAME);

	for (int i = 0; i < NUM_COURSES; i++) {
		fwrite(blank_course, sizeof(Course), 1, datfile);
	}
	free(blank_course);

}

/* reads through a text file an creates a database */
void create_file_database(FILE* datfile) {

	char filename[FILENAME_LENGTH];
	printf("Enter text file name: ");
	scanf("%s", filename);
	printf("The file name you entered is: %s\n", filename);
	FILE* txtfile;

	if ((txtfile = fopen(filename, "r"))) {

		float credits;
		char div[3], name[COURSE_LENGTH+1];
		int dept, num;
		int count = 0;
		int c = ' ';

		Course* course = NULL;

		while (fscanf(txtfile, "%2s.%d.%d %f ", div, &dept, &num, &credits) != EOF) {

			while (count < COURSE_LENGTH && (c = fgetc(txtfile)) != '\n') {
			    name[count++] = (char) c;
			}

			name[count] = '\0';
			while (c != '\n') {
				c = fgetc(txtfile); 
			}

			if ((dept < 1) || (dept > 700) || (num < 100) || (num > 899)) {
				print_err("Invalid department/number.");
			} else if (credits < 0.5 || credits > 5.5) {
				print_err("Invalid number of credits.");
			} else {
				Course* course = create_course(div, dept, num, credits, name);
				fseek(datfile, get_course_index(course->dept, course->num) * sizeof(Course), SEEK_SET);
				fwrite(course, sizeof(Course), 1, datfile);		
			}	

			/* reset count and c */
			count = 0;
			c = ' ';

		}

		free(course);

		fclose(txtfile);
		printf("Database created.\n\n");

	} else {
		print_err("That file is invalid.");
	}

}

/* adds a course from stdin to the database */
void add_to_database(FILE* datfile) {

	printf("Enter division, course number, credits, and the course name (DD.ddd.nnn x.x course_name): ");

	float credits;
	char div[3], name[COURSE_LENGTH+1];
	int dept, num;
	int count = 0;
	char c;

	scanf("%2s.%d.%d %f ", div, &dept, &num, &credits);

	while ((count < COURSE_LENGTH) && ((c = getchar()) != '\n')) {
	    name[count++] = c;
	}

	name[count] = '\0';

	if ((dept < 1) || (dept > 700) || (num < 100) || (num > 899)) {
		print_err("Invalid department/number.");
	} else if (credits < 0.5 || credits > 5.5) {
		print_err("Invalid number of credits.");
	} else {
		Course* course = create_course(div, dept, num, credits, name);
		fseek(datfile, get_course_index(course->dept, course->num) * sizeof(Course), SEEK_SET);
		fwrite(course, sizeof(Course), 1, datfile);
		free(course);
	}

}

/* displays all the details of a particular course using input for the course number (ddd.nnn format) */
void display_one(FILE* datfile) {

	int dept;
	int num;

	printf("Enter course number (ddd.nnn): ");
	scanf("%d.%d", &dept, &num);

	if (course_exists(datfile, dept, num)) {

		Course *cptr = malloc(sizeof(Course));
		fseek(datfile, get_course_index(dept, num) * sizeof(Course), SEEK_SET);
		fread(cptr, sizeof(Course), 1, datfile);

		printf("Division: %s\n", cptr->div);
		printf("Course: %d.%d\n", cptr->dept, cptr->num);
		printf("Credits: %.1f\n", cptr->credits);
		printf("Name: %s\n", cptr->name);
		printf("Course Index: %d\n", get_course_index(cptr->dept, cptr->num));
		printf("\n");

		free(cptr);

	} else {
		print_err("That course does not exist.");
	}

}

/* displays all the non-blank courses in the database */
void display_all(FILE* datfile) {

	Course *cptr = malloc(sizeof(Course));

	for (int i = 0; i < NUM_COURSES; i++) {

		fseek(datfile, i * sizeof(Course), SEEK_SET);
		fread(cptr, sizeof(Course), 1, datfile);

		if (cptr->dept != 0) {
			printf("%s.%d.%d %.1f %s\n", cptr->div, cptr->dept, cptr->num, cptr->credits, cptr->name);
		}

	}
	printf("\n");
	free(cptr);

}

/* changes the name of an existing course */
void change_name(FILE* datfile) {

	int dept, num;
	char name[COURSE_LENGTH];
	int count = 0;
	char c;

	printf("Enter course number (ddd.nnn) followed by the new name: ");
	scanf("%d.%d ", &dept, &num);

	while ((count < COURSE_LENGTH) && ((c = getchar()) != '\n')) {
	    name[count++] = c;
	}

	name[count] = '\0';

	if (course_exists(datfile, dept, num)) {

		Course *cptr = malloc(sizeof(Course));
		fseek(datfile, get_course_index(dept, num) * sizeof(Course), SEEK_SET);
		fread(cptr, sizeof(Course), 1, datfile);

		unsigned long i;
		for (i = 0; i < strlen(name); i++) {
        	cptr->name[i] = name[i];
    	}
    	cptr->name[i] = '\0';

    	fseek(datfile, get_course_index(dept, num) * sizeof(Course), SEEK_SET);
    	fwrite(cptr, sizeof(Course), 1, datfile);
	free(cptr);

	} else {
		print_err("That course does not exist.");
	}

}

void delete_from_database(FILE* datfile) {
	
	int dept;
	int num;

	printf("Enter course number (ddd.nnn): ");
	scanf("%d.%d", &dept, &num);

	if (course_exists(datfile, dept, num)) {
		Course* blank_course = create_course(BLANK_DIV, BLANK_DEPT, BLANK_NUM, BLANK_CREDITS, BLANK_NAME);
		fseek(datfile, get_course_index(dept, num) * sizeof(Course), SEEK_SET);
		fwrite(blank_course, sizeof(Course), 1, datfile);
		free(blank_course);
	} else {
		print_err("That course does not exist.");
	}

}
