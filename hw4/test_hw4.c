/**
 * 	Hugh Han
 *	EN.600.120 Intermediate Programming, Spring 2015
 * 	HW 4: test_hw4.c
 *	
 *	March 10, 2015
 *	Phone: (516) 707-1344
 *	JHED ID: hhan17
 *	JHU Email: hhan17@jhu.edu
 */

#include "hw4.h"
#include <assert.h>

int main(void) {

	FILE *datfile = fopen("test.dat", "w+b");

	Node *head[NUM_SEMESTERS];
	for (int i = 0; i < NUM_SEMESTERS; i++) {
		head[i] = NULL;
	}

	create_blank_database(datfile);
	create_file_database(datfile);
	
	display_one(datfile);
	display_all(datfile);

	change_name(datfile);
	display_all(datfile);

	delete_from_database(datfile);
	display_all(datfile);

	add_to_database(datfile);
	display_all(datfile);

	Course* csci_107 = create_course("EN", 600, 107, 3.0, "Introductory Programming");
	Course* csci_120 = create_course("EN", 600, 120, 4.0, "Intermediate Programming");
	Course* csci_226 = create_course("EN", 600, 226, 4.0, "Data Structures");
	
	Course* swag_899 = create_course("SW", 700, 899, 5.5, "Big Swag");
	Course* swag_100 = create_course("SW", 700, 100, 0.5, "Little Swag");
	Course* sweg_899 = create_course("SW", 001, 899, 5.5, "Giant Sweg");
	Course* sweg_100 = create_course("SW", 001, 100, 0.5, "Tiny Sweg");

	Course* badd_100 = create_course("SW", 999, 100, 0.5, "Bad Department");
	Course* badd_999 = create_course("SW", 100, 999, 0.5, "Bad Course Number");
	Course* badd_ccc = create_course("SW", 100, 999, 0.0, "Bad Credits");

	assert(get_course_index(csci_107->dept, csci_107->num) == get_course_index(600, 107));
	assert(get_course_index(csci_120->dept, csci_120->num) == get_course_index(600, 120));
	assert(get_course_index(csci_226->dept, csci_226->num) == get_course_index(600, 226));

	assert((get_course_index(swag_899->dept, swag_899->num) >= 0) && (get_course_index(swag_899->dept, swag_899->num) < 560000));
	assert((get_course_index(swag_100->dept, swag_100->num) >= 0) && (get_course_index(swag_100->dept, swag_100->num) < 560000));
	assert((get_course_index(sweg_899->dept, sweg_899->num) >= 0) && (get_course_index(sweg_899->dept, sweg_899->num) < 560000));
	assert((get_course_index(sweg_100->dept, sweg_100->num) >= 0) && (get_course_index(sweg_100->dept, sweg_100->num) < 560000));

	assert(get_course_index(badd_999->dept, badd_999->num) == get_course_index(badd_ccc->dept, badd_ccc->num));
	assert(get_course_index(badd_999->dept, badd_999->num) != get_course_index(badd_100->dept, badd_100->num));
	assert(get_course_index(badd_100->dept, badd_100->num) > 560000);

	Node* semesters[NUM_SEMESTERS];
	for (int i = 0; i < NUM_SEMESTERS; i++) {
 		semesters[i] = NULL;
 	}

 	insert(datfile, &semesters[0]);
 	insert(datfile, &semesters[0]);
 	insert(datfile, &semesters[0]);

 	insert(datfile, &semesters[1]);
 	insert(datfile, &semesters[1]);
 	insert(datfile, &semesters[1]);

 	insert(datfile, &semesters[4]);
 	insert(datfile, &semesters[4]);
 	insert(datfile, &semesters[4]);

 	insert(datfile, &semesters[6]);
 	insert(datfile, &semesters[6]);
 	insert(datfile, &semesters[6]);

 	insert(datfile, &semesters[9]);
 	insert(datfile, &semesters[9]);
 	insert(datfile, &semesters[9]);

 	printf("Course index of 600.120 is: %d\n", get_course_index(600, 120));
 	printf("Course index of 001.100 is: %d\n", get_course_index(001, 100));
 	printf("Course index of 700.899 is: %d\n", get_course_index(700, 899));

    for (int i = 0; i < NUM_SEMESTERS; i++) {
    	printf("Printing semester %d...\n", i);
    	print_list(&semesters[i]);
    	printf("\n");
    }

	print_menu();
	interpret_choice(datfile, semesters);

	fclose(datfile);

}
