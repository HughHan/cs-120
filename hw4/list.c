/**
 *  Hugh Han
 *  EN.600.120 Intermediate Programming, Spring 2015
 *  HW 4: list.c
 *  
 *  March 10, 2015
 *  Phone: (516) 707-1344
 *  JHED ID: hhan17
 *  JHU Email: hhan17@jhu.edu
 */

#include "hw4.h"

Course* create_course(char* div, int dept, int num, float credits, char* name) {
    
    Course* course = malloc(sizeof(Course));
    for (unsigned long i = 0; i < strlen(div); i++) {
        course->div[i] = div[i];
    }
    course->dept = dept;
    course->num = num;
    course->credits = credits;
    for (unsigned long i = 0; i < strlen(name); i++) {
        course->name[i] = name[i];
    }

    return course;
}

/* count and return the number of elements in the List */
long size(List nptr) {
    if (nptr == NULL)
        return 0;
    else
        return 1+size(nptr->next);
}

/* prints the list */
void print_list(List list_array[NUM_SEMESTERS]) {

    int sem;

    printf("Enter a semester between 0-9: ");
    scanf("%d", &sem);
    printf("\n");

    List nptr = list_array[sem];

    while (nptr != NULL) {
        printf("%s.%d.%d %s %.1f %s\n", nptr->course->div, nptr->course->dept, nptr->course->num, nptr->course->name, nptr->course->credits, nptr->grade);
        nptr = nptr->next;
    }
    printf("\n");

}

/* adds to the front of the list */
void add_to_front(List* lptr, Course *cptr, char* grade) {
    Node *n = malloc(sizeof(Node));
    n->course = cptr;
    n->grade = grade;
    n->next = (*lptr);
    *lptr = n;
}

/* deletes an item from the list recursively */
int delete_from_list(List list_array[NUM_SEMESTERS]) {

    int sem;
    int dept, num;

    printf("Enter a semester between 0-9: ");
    scanf("%d", &sem);
    printf("\n");

    printf("Enter course number (ddd.nnn): ");
    scanf("%d.%d", &dept, &num);

    List* lptr = &(list_array[sem]);
    return delete_from_listH(lptr, dept, num);

}

/* helper method for delete_from_list */
int delete_from_listH(List* list_ptr, int dept, int num) {

    if (*list_ptr == NULL)
        return 0;
    if (((*list_ptr)->course->dept == dept) && ((*list_ptr)->course->num == num)) {
        Node *temp = *list_ptr;
        *list_ptr = (*list_ptr)->next;
        free(temp);
        return 1;
    } else {
        return delete_from_listH((&(*list_ptr)->next), dept, num);
    }

}

/* get rid of entire list */
void clear_list(List* lptr) {
    if (*lptr != NULL) {
        clear_list(&((*lptr)->next));
        free(*lptr);
        *lptr = NULL;
    }

}

/* find a value in the list, return pointer to containing node, or NULL */
Node* find(List nptr, Course *cptr)
{
    while ((nptr) != NULL) {
        if ((nptr->course->dept == cptr->dept) && (nptr->course->num == cptr->num))
            return nptr;
        else
            nptr = nptr->next;
    }
    return nptr;
}

/* replace first occurrence of old with new, return 1 success, 0 failure */
int replace(List list, Course *c_old, Course *c_new) {
    Node* to_change = find(list, c_old);
    if (to_change) {
        to_change->course = c_new;
        return 1;
    } else {
        return 0;
    }
}

/* insert in order if not duplicate, assumes list is ordered */
Node* insert(FILE* datfile, List list_array[NUM_SEMESTERS]) {

    int sem;
    int dept, num;
    char grade[3];
    int count = 0;
    char c;

    printf("Enter a semester between 0-9: ");
    scanf("%d", &sem);
    printf("\n");

    if (sem < 0 || sem > 9) {
        print_err("Invalid semester.");
        return NULL;
    }

    printf("Enter course number and letter grade (ddd.nnn gg): ");
    scanf("%d.%d ", &dept, &num);
    while ((count < 3) && ((c = getchar()) != '\n')) {
        grade[count++] = c;
    }
    grade[count] = '\0';
    printf("\n");

    bool plus_minus;
    switch (grade[0]) {
        case 'A': plus_minus = true; break;
        case 'B': plus_minus = true; break;
        case 'C': plus_minus = true; break;
        case 'D': plus_minus = true; break;
        case 'F': plus_minus = false; break;
        case 'I': plus_minus = false; break;
        case 'S': plus_minus = false; break;
        case 'U': plus_minus = false; break;
        default: 
            print_err("Invalid letter grade."); 
            return NULL; 
            break;
    }

    switch (grade[1]) {
        case '+': 
            if (!plus_minus) {
                print_err("That letter grade cannot have +/-."); 
                return NULL;
            }
            break;
        case '-': 
            if (!plus_minus) {
                print_err("That letter grade cannot have +/-."); 
                return NULL;
            }
            break;
        case '/': break;
        default:
            print_err("Invalid symbols."); 
            return NULL; 
            break;
    }

    Node* n = malloc(sizeof(Node));
    n->course = malloc(sizeof(Course));
    n->grade = malloc(3 * sizeof(char));
    for (int i = 0; i < 3; i++) {
        n->grade[i] = grade[i];
    }
    n->next = NULL;

    fseek(datfile, get_course_index(dept, num) * sizeof(Course), SEEK_SET);
    fread(n->course, sizeof(Course), 1, datfile);

    if ((n->course->dept < 1) || (n->course->dept > 700) || (n->course->num < 100) || (n->course->num > 899)) {
        print_err("Invalid department/number.");
        return NULL;
    } else {

        List* lptr = &list_array[sem];
        Node* curr = *lptr;

        if ((*lptr == NULL) || (get_course_index(dept, num) < get_course_index(lptr[sem]->course->dept, lptr[sem]->course->num))) {
            add_to_front(lptr, n->course, n->grade);
            printf("Course added.\n");
        } else {

            while ((curr->next != NULL) && (get_course_index(dept, num) > get_course_index(curr->next->course->dept, curr->next->course->num))) {           
                curr = curr->next;
            }
            if ((curr->next != NULL) && (get_course_index(dept, num) == get_course_index(curr->next->course->dept, curr->next->course->num))) {
                print_err("That course already exists.\n");
                return NULL;
            } else if ((curr->next != NULL) && (get_course_index(dept, num) == get_course_index(curr->course->dept, curr->course->num))) {
                print_err("That course already exists.\n");
                return NULL;
            } else if ((curr->next == NULL) && (get_course_index(dept, num) == get_course_index(curr->course->dept, curr->course->num))) {
                print_err("That course already exists.\n");
                return NULL;
            } else {
                n->next = curr->next;
                curr->next = n;
                printf("Course added.\n");
            }                

        }

    }

    return n;
}

List* merge_lists(List list_array[NUM_SEMESTERS]) {

    /**     PSEUDOCODE FOR MERGE_LISTS      **/
    //  iterate through lists in array {
    //      iterate through courses in list {
    //          iterate through courses in result {
    //              compare courses in list and result
    //              add the smaller one into the result
    //          }
    //      }
    //  }
    //  return the result

    List* result = NULL;
    Node* nptr = malloc(sizeof(Node));
    Node* rptr = malloc(sizeof(Node));
    bool added = false;

    for (int i = 0; i < NUM_SEMESTERS; i++) {

        nptr = list_array[i]; // sets the pointer to the first element in the current list

        if (list_array[i] != NULL) {

            if (*result == NULL) {
                result = malloc(sizeof(List));
                *result = nptr;
            } else {

                /* while the course in the semester is less than the course in the result list, advance one in the result list. */
                while (nptr->next != NULL) {

                    rptr = *result; // sets the pointer to the first element in the result list

                    while (rptr->next != NULL) {
                        if (get_course_index(nptr->course->dept, nptr->course->num) > get_course_index(rptr->course->dept, rptr->course->num)) {
                            nptr->next = rptr->next;
                            rptr->next = nptr;
                            added = true;
                            break;
                        } 
                        rptr = rptr->next;
                    }
                    if (!added) {
                        // if all the courses in the semester have been iterated through but none were added to the result, then add to the front.
                        Course* new_course = create_course(nptr->course->div, nptr->course->dept, nptr->course->num, nptr->course->credits, nptr->course->name);
                        add_to_front(result, new_course, nptr->grade);
                    }
                    nptr = nptr->next;
                }
            }

        }
    }

    rptr = *result; // sets the pointer to the first element in the result list
    while (rptr != NULL) {
        printf("%s.%d.%d %s %.1f %s\n", rptr->course->div, rptr->course->dept, rptr->course->num, rptr->course->name, rptr->course->credits, rptr->grade); // SEGMENTATION FAULT
        printf("HI\n");
        rptr = rptr->next;
    }
    printf("\n");

    return result;
}

int compute_gpa(List list_array[NUM_SEMESTERS]) {

    int sem;
    int total_credits = 0;
    float gpa = 0;

    printf("Enter a semester between 0-9: ");
    scanf("%d", &sem);
    printf("\n");

    Node* nptr = list_array[sem];

    while (nptr != NULL) {
        printf("%s\n", nptr->grade);
        if (strcmp(nptr->grade, "A+") == 0 || strcmp(nptr->grade, "A/") == 0) {
            gpa += (4.0 * (nptr->course->credits));
            total_credits += nptr->course->credits;
        } else if (strcmp(nptr->grade, "A-") == 0) {
            gpa += (3.7 * (nptr->course->credits));
            total_credits += nptr->course->credits;
        } else if (strcmp(nptr->grade, "B+") == 0) {
            gpa += (3.3 * (nptr->course->credits));
            total_credits += nptr->course->credits;
        } else if (strcmp(nptr->grade, "B/") == 0) {
            gpa += (3.0 * (nptr->course->credits));
            total_credits += nptr->course->credits;
        } else if (strcmp(nptr->grade, "B-") == 0) {
            gpa += (2.7 * (nptr->course->credits));
            total_credits += nptr->course->credits;
        } else if (strcmp(nptr->grade, "C+") == 0) {
            gpa += (2.3 * (nptr->course->credits));
            total_credits += nptr->course->credits;
        } else if (strcmp(nptr->grade, "C/") == 0) {
            gpa += (2.0 * (nptr->course->credits));
            total_credits += nptr->course->credits;
        } else if (strcmp(nptr->grade, "C-") == 0) {
            gpa += (1.7 * (nptr->course->credits));
            total_credits += nptr->course->credits;
        } else if (strcmp(nptr->grade, "D+") == 0) {
            gpa += (1.3 * (nptr->course->credits));
            total_credits += nptr->course->credits;
        } else if (strcmp(nptr->grade, "D/") == 0) {
            gpa += (1.0 * (nptr->course->credits));
            total_credits += nptr->course->credits;
        } else if (strcmp(nptr->grade, "D-") == 0) {
            gpa += (0.7 * (nptr->course->credits));
            total_credits += nptr->course->credits;
        } else if (strcmp(nptr->grade, "F/") == 0) {
            gpa += (0.0 * (nptr->course->credits));
            total_credits += nptr->course->credits;
        } else if (strcmp(nptr->grade, "I/") == 0 || strcmp(nptr->grade, "S/") == 0 || strcmp(nptr->grade, "U/") == 0) {
            /* do nothing */
        }
        nptr = nptr->next;
    }
    if (total_credits == 0) {
        print_err("There are no courses with valid grades this semester.");
        return 1;
    } else {
        gpa = gpa / total_credits;
    }
    printf("%.1f", gpa);
    return 0;

}
