/**
 *  Hugh Han
 *  EN.600.120 Intermediate Programming, Spring 2015
 *  HW 4: hw4.c
 *  
 *  March 10, 2015
 *  Phone: (516) 707-1344
 *  JHED ID: hhan17
 *  JHU Email: hhan17@jhu.edu
 */

#include "hw4.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main(int argc, char* argv[]) {

 	if (argc == 1) {
 		printf("Usage: ./hw4\n");
 	} else {

 		/* Create the array of linked lists and initialize to NULL */
 		List list_array[NUM_SEMESTERS];
 		for (int i = 0; i < NUM_SEMESTERS; i++) {
 			list_array[i] = NULL;
 		}

 		FILE *datfile;

 		if ((datfile = fopen(argv[1], "r+b"))) {
 			// file exists
        	printf("File opened.\n");
 		} else {
 			// file does not exist
 			printf("That file does not exist. Creating file...\n");
 			datfile = fopen(argv[1], "w+b");
 			create_blank_database(datfile);
 		}

 		bool q;
 		do {
	 		print_menu();
	 		q = interpret_choice(datfile, list_array);
	 	} while (q);

 		/* close the database file */
 		printf("Closing the data file...\n");
 		fclose(datfile);

 	}

}
 
