/**
 * 	Hugh Han
 *	EN.600.120 Intermediate Programming, Spring 2015
 * 	HW 4: hw4.h
 *	
 *	March 10, 2015
 *	Phone: (516) 707-1344
 *	JHED ID: hhan17
 *	JHU Email: hhan17@jhu.edu
 */

#ifndef HW4_H
#define HW4_H

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define NUM_SEMESTERS 10
#define NUM_COURSES 560000
#define COURSE_LENGTH 30
#define SHORT_BUFFER 8

#define BLANK_DIV ""
#define BLANK_DEPT 000
#define BLANK_NUM 000
#define BLANK_CREDITS 0.0f
#define BLANK_NAME ""

struct course_ {
	char div[3];
	int dept;
	int num;
	char name[COURSE_LENGTH+1];
	float credits;
};

struct node {
	struct course_* course;
	char* grade;
	struct node* next;
};

typedef struct course_ Course;
typedef struct node Node;
typedef Node* List;

/***************** list.c *****************/

Course* create_course(char* div, int dept, int num, float credits, char* name);

long size(List l); 

void print_list(List list_array[NUM_SEMESTERS]); 

void add_to_front(List* lptr, Course* cptr, char* c); 

int delete_from_list(List list[NUM_SEMESTERS]);
int delete_from_listH(List* lptr, int dept, int num);
void clear_list(List* lptr);

Node* find(List l, Course* c);
int replace(List l, Course* c_old, Course* c_new);
Node* insert(FILE* datfile, List list_array[NUM_SEMESTERS]);

List* merge_lists(List list_array[NUM_SEMESTERS]);
int compute_gpa(List list_array[NUM_SEMESTERS]);

/***************** database.c *****************/

int get_course_index(int dept, int num);
bool course_exists(FILE* datfile, int dept, int num);

void create_blank_database(FILE* datfile);
void create_file_database(FILE* datfile);
void add_to_database(FILE* datfile);
void display_one(FILE* datfile);
void display_all(FILE* datfile);
void change_name(FILE* datfile);
void delete_from_database(FILE* datfile);

/***************** menu.c *****************/

void print_menu();
bool interpret_choice(FILE* datfile, List list_array[NUM_SEMESTERS]);

/***************** err.c *****************/

void print_err(char* message);

#endif
