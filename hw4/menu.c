/**
 * 	Hugh Han
 *	EN.600.120 Intermediate Programming, Spring 2015
 * 	HW 4: menu.c
 *	
 *	March 10, 2015
 *	Phone: (516) 707-1344
 *	JHED ID: hhan17
 *	JHU Email: hhan17@jhu.edu
 */

#include "hw4.h"
#include <stdio.h>

/* prints the list of options to the screen. */
void print_menu() {
	printf(
		"0.  Quit.\n"
		"1.  Create new courses from a plain text file and add them to the database.\n"
		"2.  Add a single course to the database.\n"
		"3.  Display all the details of a particular course.\n"
		"4.  Display (on the screen) all the actual courses in the database (not blank records).\n"
		"5.  Change the name of a course.\n"
		"6.  Delete a course from the database.\n"
		"7.  Add a course to a semester course listing.\n"
		"8.  Remove a course from a semester course listing.\n"
		"9.  Display a semester course listing on the screen, in course number order.\n"
		"10. Create and display a merged ordered listing of all the courses in all the semesters.\n"
		"11. Compute and display the GPA for a particular semester list.\n"
	);
}

/* interprets the user's choice. */
bool interpret_choice(FILE* datfile, List list_array[NUM_SEMESTERS]) {
	int choice;
	scanf("%d", &choice);
	printf("Your choice was: %d\n", choice);				// DEBUG PRINT STATEMENT
	switch (choice) {
		case 0: 
			for (int i = 0; i < NUM_SEMESTERS; i++) {
				clear_list(&list_array[i]);
			}
			return false; 
			break;
		case 1: create_file_database(datfile); 	break;
		case 2: add_to_database(datfile); 		break;
		case 3: display_one(datfile); 			break;
		case 4: display_all(datfile); 			break;
		case 5: change_name(datfile);			break;
		case 6: delete_from_database(datfile); 	break;
		case 7: insert(datfile, list_array); 	break;
		case 8: delete_from_list(list_array); 	break;
		case 9: print_list(list_array); 		break;
		case 10: merge_lists(list_array);		break;
		case 11: compute_gpa(list_array);		break;
		default: 
			print_err("Invalid choice.");
			break;
	}

	return true;
	
}
