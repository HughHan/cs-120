/**
 * 	Hugh Han
 *	EN.600.120 Intermediate Programming, Spring 2015
 * 	HW 4: err.c
 *	
 *	March 4, 2015
 *	Phone: (516) 707-1344
 *	JHED ID: hhan17
 *	JHU Email: hhan17@jhu.edu
 */

#include <stdio.h>

/* prints an error message */
void print_err(char* message) {
	fprintf(stderr, "%s\n", message);
}
